import $ from 'jquery';
import { Dialog } from "./dialog_base";

export class EnvDialog extends Dialog<{ [key: string]: string }> {
	private table: JQuery;
	private row_add: JQuery;

	private next_row_id: number;

	constructor() {
		super($('#dialog_env'));
		this.table = this.element.find("#dialog_env_table");
		this.row_add = this.table.find("#dialog_env_add_row");
		this.next_row_id = 0;
	}

	show(): void {
		super.show();
		this.row_add.find(".add").on('click', () => { this.add_row("", ""); });
	}

	hide(): void {
		super.hide();
		this.clear();
		this.row_add.find(".add").off('click');
	}

	clear(): void {
		this.table.find(".dialog_env_row").remove();
	}

	add_row(key: string, value: string): void {
		const row_id = this.next_row_id++;

		const row = $("<tr/>", {
			class: "dialog_env_row",
			"data-row-id": row_id
		});

		$("<td/>").append($("<input>", {
			type: "text",
			value: key
		})).appendTo(row);
		$("<td/>").append($("<input>", {
			type: "text",
			value: value
		})).appendTo(row);
		$("<td/>").append($("<button/>", {
			class: "remove",
			type: "button",
			click: () => { this.remove_row(row_id); }
		})).appendTo(row);

		this.row_add.before(row);
	}

	remove_row(row_id: number): void {
		this.table.find("tr[data-row-id=" + row_id + "]").remove();
	}

	set_data(data: { [key: string]: string }): void {
		this.clear();
		for (const [key, value] of Object.entries(data)) {
			this.add_row(key, value);
		}
	}

	get_data(): { [key: string]: string } {
		const output: { [key: string]: string } = {};
		this.table.find(".dialog_env_row").each((index, element) => {
			const key = $(element).find('td:nth-child(1) input').val()?.toString();
			const value = $(element).find('td:nth-child(2) input').val()?.toString();
			if (key != null && key.trim() != "" && value != null)
				output[key] = value;
			else
				console.error("Cannot parse row " + index);
		});
		return output;
	}
}