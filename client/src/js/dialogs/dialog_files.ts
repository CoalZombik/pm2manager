import $ from 'jquery';
import { Dialog, DialogResponseType } from './dialog_base';
import path from 'path';
import { ClientApi } from '../client_api';
import * as schema from 'pm2manager_schema';
import { format_memory, PATH_TYPE_TEXT, show_error } from '../utils';
import JSZip from 'jszip';
import { InputDialog, InputDialogProp } from './dialog_input';
import { ProgressDialog } from './dialog_progress';

export class FilesDialog extends Dialog<void> {
	readonly process_id: number;

	private current_dir: string;

	private readonly api: ClientApi;

	private data: schema.PathInfo[];
	private move_files: schema.PathInfo[] | null;

	private readonly button_up: JQuery;
	private readonly div_path: JQuery;

	private readonly button_download: JQuery;
	private readonly button_upload: JQuery;
	private readonly button_move: JQuery;
	private readonly button_move_place: JQuery;
	private readonly button_remove: JQuery;
	private readonly button_refresh: JQuery;
	private readonly button_new_directory: JQuery;

	private readonly context_menu: JQuery;
	private readonly button_context_menu_download: JQuery;
	private readonly button_context_menu_rename: JQuery;
	private readonly button_context_menu_remove: JQuery;

	private readonly table_files: JQuery;

	private readonly download_link: JQuery;
	private readonly file_selector: JQuery;

	constructor(process_id: number, api: ClientApi, dir?: string) {
		super($("#dialog_files"));

		this.process_id = process_id;
		this.current_dir = dir ?? '.';
		this.api = api;
		this.button_up = this.element.find("#dialog_files_up");
		this.div_path = this.element.find("#dialog_files_path");
		this.button_download = this.element.find("#dialog_files_download");
		this.button_upload = this.element.find("#dialog_files_upload");
		this.button_move = this.element.find("#dialog_files_move");
		this.button_move_place = this.element.find("#dialog_files_move_place");
		this.button_remove = this.element.find("#dialog_files_remove");
		this.button_refresh = this.element.find("#dialog_files_refresh");
		this.button_new_directory = this.element.find("#dialog_files_new_directory");

		this.context_menu = this.element.find("#dialog_files_context_menu");
		this.button_context_menu_download = this.context_menu.find("#dialog_files_context_menu_download");
		this.button_context_menu_rename = this.context_menu.find("#dialog_files_context_menu_rename");
		this.button_context_menu_remove = this.context_menu.find("#dialog_files_context_menu_remove");

		this.table_files = this.element.find("#dialog_files_list>tbody");

		this.download_link = this.element.find("#dialog_files_download_link");
		this.file_selector = this.element.find("#dialog_files_file_selector");

		this.data = [];
		this.move_files = null;
	}

	show(): void {
		super.show();

		$("#dialog_layer").on('click', () => { this.hide_context_menu(); });

		this.button_up.on('click', () => { this.directory_up(); });
		this.button_download.on('click', () => { this.download_checked().then((blob) => { this.save_blob(blob, "files.zip"); }).catch(show_error); });
		this.button_upload.on('click', () => { this.upload_picker(); });
		this.button_move.on('click', () => { this.select_checked(); });
		this.button_move_place.on('click', () => { this.place_selected(); });
		this.button_remove.on('click', () => { this.remove_checked(); });
		this.button_refresh.on('click', () => { this.directory_load(this.current_dir); });
		this.button_new_directory.on('click', () => { this.new_directory_dialog(); });

		this.button_context_menu_download.on('click', () => { this.context_menu_download(); });
		this.button_context_menu_rename.on('click', () => { this.context_menu_rename(); });
		this.button_context_menu_remove.on('click', () => { this.context_menu_remove(); });

		this.refresh();
	}

	hide(): void {
		super.hide();

		$("#dialog_layer").off();

		this.button_up.off('click');
		this.button_download.off('click');
		this.button_upload.off('click');
		this.button_move.off('click');
		this.button_move_place.off('click');
		this.button_remove.off('click');
		this.button_refresh.off('click');
		this.button_new_directory.off('click');

		this.button_context_menu_download.off('click');
		this.button_context_menu_rename.off('click');
		this.button_context_menu_remove.off('click');
	}

	/**
	 * Update directory data from received response data
	 */
	update(data: schema.ResponseListDirectoryData): void {
		if (data.process_id != this.process_id)
			throw new Error("Received process_id is not equal!");

		this.current_dir = data.path;
		this.data = data.content;

		this.draw();
	}

	/**
	 * Draw current directory data and path to HTML
	 */
	draw(): void {
		this.div_path.text(this.current_dir);

		this.table_files.find("tr").remove();

		this.data.forEach((info, index) => {
			// show symlink destination with custom name
			let show_info: schema.PathInfoBase = info;
			let display_name = path.basename(show_info.path);
			if (show_info.type == schema.PathType.symbolic_link && info.symlink != null) {
				show_info = {
					...info.symlink,
					path: info.path
				};
				display_name += ' -> ' + info.symlink.path;
			}

			const row = $('<tr/>', {
				"data-file-index": index
			});
			row.on('contextmenu', (event) => {
				event.preventDefault();
				this.show_context_menu(index, event.pageY, event.pageX);
			});

			const name_button = $('<button/>', {
				type: 'button'
			}).text(display_name);

			if (show_info.type == schema.PathType.directory)
				name_button.on('click', () => { this.directory_load(show_info.path); });
			else if (show_info.type == schema.PathType.regular_file)
				name_button.on('click', () => { this.download_specific(show_info.path).then((blob) => { this.save_blob(blob, path.basename(show_info.path)); }); });

			$('<td/>').append($('<input>', {
				type: 'checkbox',
				id: 'dialog_files_item' + index,
				"data-file-index": index
			})).append(name_button).appendTo(row);

			$('<td/>').text(PATH_TYPE_TEXT[show_info.type]).appendTo(row);

			const permissions = show_info.size < 0 ? "" : show_info.permission.toString(8) + " " + show_info.uid + ":" + show_info.gid;
			$('<td/>').text(permissions).appendTo(row);

			$('<td/>').text(show_info.type != schema.PathType.regular_file || show_info.size < 0 ? "" : format_memory(show_info.size)).appendTo(row);
			$('<td/>').text(show_info.size < 0 ? "" : new Date(show_info.modify_time * 1000).toLocaleString()).appendTo(row);
			$('<td/>').text(show_info.size < 0 ? "" : new Date(show_info.create_time * 1000).toLocaleString()).appendTo(row);

			this.table_files.append(row);
		});

		if (this.move_files == null) {
			this.button_move.show();
			this.button_move_place.hide();
		} else {
			this.button_move.hide();
			this.button_move_place.show();
		}

		this.hide_context_menu();
	}

	/**
	 * Load parent directory
	 */
	directory_up(): void {
		const new_dir = path.dirname(this.current_dir);
		console.log("old: " + this.current_dir + ", new: " + new_dir);
		this.directory_load(new_dir);
	}

	/**
	 * Refresh current directory data
	 */
	refresh(): void {
		this.directory_load(this.current_dir);
	}

	/**
	 * Load directory data from api
	 */
	directory_load(new_dir: string): void {
		this.api.list_directory(this.process_id, path.normalize(new_dir)).then((data) => { this.update(data); }).catch(show_error);
	}

	/**
	 * @returns List of checked files
	 */
	private get_checked(): schema.PathInfo[] {
		const data = this.data;
		const output: schema.PathInfo[] = [];

		this.table_files.find("input:checked").each((index, element) => {
			output.push(data[Number($(element).attr('data-file-index'))]);
		});

		console.log("get_checked: ");
		console.log(output);

		return output;
	}

	/**
	 * Load content of specified directory and all subdirectories
	 */
	private async recursive_directory_load(directory: string): Promise<schema.PathInfo[]> {
		const loaded = await this.api.list_directory(this.process_id, directory);

		const files: schema.PathInfo[] = [];
		const promises: Promise<schema.PathInfo[]>[] = [];

		for (let i = 0; i < loaded.content.length; i++) {
			const file = loaded.content[i];
			if (file.type == schema.PathType.directory)
				promises.push(this.recursive_directory_load(file.path));
			else
				files.push(file);
		}

		return files.concat(...await Promise.all(promises));
	}

	/**
	 * Download all checked files/directories and pack it as zip.
	 */
	download_checked(): Promise<Blob> {
		const checked = this.get_checked();
		return this.download_zip(checked);
	}

	/**
	 * Download specified files/directories and pack it as zip.
	 */
	async download_zip(selected_files: schema.PathInfo[]): Promise<Blob> {
		const zip = new JSZip();

		// load all files in directory
		let files: schema.PathInfo[] = [];
		const promises: Promise<schema.PathInfo[]>[] = [];
		for (let i = 0; i < selected_files.length; i++) {
			const file = selected_files[i];
			if (file.type == schema.PathType.directory)
				promises.push(this.recursive_directory_load(file.path));
			else
				files.push(file);
		}
		files = files.concat(...await Promise.all(promises)).filter((value) => value.type == schema.PathType.directory || value.type == schema.PathType.regular_file);

		// compute total size
		let full_downloaded_size = 0;
		let total_size = 0;
		for (let i = 0; i < files.length; i++) {
			total_size += files[i].size;
		}

		const progress_dialog = new ProgressDialog("Downloading " + files.length + " files...");
		progress_dialog.show();

		try {
			for (let i = 0; i < files.length; i++) {
				const blob = await this.download_specific(files[i].path, (downloaded) => {
					progress_dialog.update_progress((full_downloaded_size + downloaded) * 100 / total_size);
				});

				full_downloaded_size += blob.size;
				zip.file(files[i].path, blob);
			}
		} finally {
			progress_dialog.hide();
		}

		return zip.generateAsync({ type: 'blob' });
	}

	/**
	 * Download specific file (not directory).
	 * @param progress_callback callback that is called after each file chunk was received
	 * @returns 
	 */
	async download_specific(file_path: string, progress_callback?: (downloaded: number, total: number) => void): Promise<Blob> {
		const progress_dialog = new ProgressDialog("Downloading '" + file_path + "'...");
		if (progress_callback == null)
			progress_dialog.show();

		const fd_data = await this.api.open_file(this.process_id, path.normalize(file_path), false);
		const buffers: Buffer[] = [];

		let downloaded_size = 0;

		await this.api.read_file_chunk(this.process_id, fd_data.fd, 0, -1, (data) => {
			if (data.buffer != null) {
				buffers.push(data.buffer);
				downloaded_size += data.buffer.byteLength;

				if (progress_callback != null)
					progress_callback(downloaded_size, fd_data.size);
				else
					progress_dialog.update_progress(downloaded_size * 100 / fd_data.size);
			}
		}).finally(() => {
			if (progress_callback == null)
				progress_dialog.hide();
			return this.api.close_file(fd_data.process_id, fd_data.fd);
		});

		return new Blob(buffers, { type: 'application/octet-stream' });
	}

	/**
	 * Send request to web browser to dowload a blob as file
	 * @param name name of file
	 */
	save_blob(blob: Blob, name: string): void {
		console.log("saving blob");
		console.log(blob);

		const url = URL.createObjectURL(blob);
		this.download_link.attr('download', name);
		this.download_link.attr('href', url);
		this.download_link[0].click();
		URL.revokeObjectURL(url);
	}

	/**
	 * Open file picker and upload selected files
	 */
	upload_picker(): void {
		this.file_selector.trigger('click');

		this.file_selector.one('change', async () => {
			const progress_dialog = new ProgressDialog("Uploading...");
			progress_dialog.show();

			const files: File[] = this.file_selector.prop('files');

			let full_uploaded_size = 0;
			let total_size = 0;
			for (let i = 0; i < files.length; i++) {
				total_size += files[i].size;
			}

			try {
				for (let i = 0; i < files.length; i++) {
					const file = files[i];
					const file_path = path.join(this.current_dir, file.name);
					await this.upload_blob(file_path, file, (uploaded) => {
						progress_dialog.update_progress((full_uploaded_size + uploaded) * 100 / total_size);
					});

					full_uploaded_size += file.size;
				}
			} catch (e: unknown) {
				show_error(String(e));
			} finally {
				progress_dialog.hide();
			}
		});
	}

	/**
	 * Upload blob file 
	 * @param file_path path of uploaded file
	 * @param blob blob to upload
	 * @param progress_callback callback that is called after each file chunk was uploaded
	 */
	async upload_blob(file_path: string, blob: Blob, progress_callback: (uploaded: number, total: number) => void): Promise<void> {
		const open_file = await this.api.open_file(this.process_id, path.normalize(file_path), true);
		try {
			const size = blob.size;
			const stream: ReadableStream<Uint8Array> = blob.stream();
			const reader = stream.getReader();

			let eof = false;
			let current_offset = 0;
			while (!eof) {
				const data = await reader.read();
				if (data.done) {
					eof = true;
					await this.api.write_file_chunk(this.process_id, open_file.fd, current_offset, 0, null, true);
					break;
				}

				const buffer = Buffer.from(data.value);
				await this.api.write_file_chunk(this.process_id, open_file.fd, current_offset, buffer.length, buffer, false);
				current_offset += buffer.length;

				progress_callback(current_offset, size);
			}
		}
		catch (e: unknown) {
			show_error(String(e));
		} finally {
			await this.api.close_file(this.process_id, open_file.fd);
			this.refresh();
		}
	}

	/**
	 * Start point of GUI file moving.
	 * Save checked files to `move_files`.
	 */
	select_checked(): void {
		this.move_files = this.get_checked();
		this.draw();
	}

	/**
	 * Move specific file/directory to new destination
	 */
	async move_specific(oldpath: string, newpath: string): Promise<void> {
		await this.api.move_path(this.process_id, oldpath, newpath);
	}

	/**
	 * Place selected files to current directory
	 */
	place_selected(): void {
		if (this.move_files == null)
			throw new Error("Trying to place null files");

		const promises: Promise<void>[] = [];
		for (let i = 0; i < this.move_files.length; i++) {
			const file = this.move_files[i];
			const basename = path.basename(file.path);
			const newpath = path.join(this.current_dir, basename);
			promises.push(this.move_specific(file.path, newpath));
		}

		Promise.all(promises).catch(show_error).then(() => {
			this.move_files = null;
			this.refresh();
		});
	}

	/**
	 * Remove specific file
	 */
	async remove_specific(file: string): Promise<void> {
		await this.api.remove_path(this.process_id, path.normalize(file), true);
	}

	/**
	 * Remove all checked files.
	 * Show dialog and remove if user accept.
	 */
	remove_checked(): void {
		const checked = this.get_checked();

		const dialog = new InputDialog({
			message: "Do you want to remove these files?<br/>" +
				checked.filter((value) => "'" + value.path + "'").join("<br/>"),
			inputs: {}
		});
		dialog.show();
		dialog.promise().then((response) => {
			if (response.type != DialogResponseType.submit)
				return;

			const promises: Promise<void>[] = [];
			for (let i = 0; i < checked.length; i++) {
				promises.push(this.remove_specific(checked[i].path));
			}

			return Promise.all(promises).finally(() => { this.refresh(); });
		}).catch(show_error).then(() => { dialog.hide(); });
	}

	/**
	 * Show dialog to enter new directory name and create this directory if dialog is confirmed
	 */
	async new_directory_dialog(): Promise<void> {
		const dialog_prop: InputDialogProp = {
			message: "Enter name of new directory",
			inputs: {
				name: { type: 'text' }
			}
		};

		const dialog = new InputDialog(dialog_prop);
		dialog.promise().then((response) => {
			if (response.type == DialogResponseType.submit)
				return this.api.create_directory(this.process_id, path.normalize(path.join(this.current_dir, response.data.name as string)), true).catch(show_error).then(() => {
					this.refresh();
				});
		}).finally(() => { dialog.hide(); });
	}

	private real_file_type(info: schema.PathInfo): schema.PathType {
		if (info.type == schema.PathType.symbolic_link && info.symlink != null)
			return info.symlink.type;
		return info.type;
	}

	/**
	 * Show context menu about specified file.
	 * @param index Index of file in `this.data` array
	 * @param top offset from top position of screen (Y axis)
	 * @param left offset from left position of screen (X axis)
	 */
	private show_context_menu(index: number, top: number, left: number): void {
		this.context_menu.css("top", top + "px");
		this.context_menu.css("left", left + "px");
		this.context_menu.attr("data-file-index", index);

		// show download button only for regular files
		const real_type = this.real_file_type(this.data[index]);
		if (real_type == schema.PathType.directory || real_type == schema.PathType.regular_file)
			this.button_context_menu_download.show();
		else
			this.button_context_menu_download.hide();

		this.context_menu.show();
	}

	/**
	 * Hide context menu.
	 * This function is save to call to already hidden menu.
	 */
	private hide_context_menu(): void {
		this.context_menu.hide();
	}

	/**
	 * Download file that have currently opened context menu
	 */
	context_menu_download(): void {
		const file_index = Number(this.context_menu.attr('data-file-index'));
		if (isNaN(file_index))
			throw new Error("Invalid context menu data-file-index");
		const file_info = this.data[file_index];

		let download_promise: Promise<Blob>;
		if (file_info.type == schema.PathType.regular_file)
			download_promise = this.download_specific(file_info.path);
		else if (file_info.type == schema.PathType.directory)
			download_promise = this.download_zip([file_info]);
		else
			download_promise = Promise.reject("Download entry that is not file or directory is not supported");

		download_promise.then((blob) => {
			this.save_blob(blob, path.basename(file_info.path));
		}).catch(show_error);
	}

	/**
	 * Rename file that have currently opened context menu.
	 * Show dialog and rename after this dialog is submitted.
	 */
	context_menu_rename(): void {
		const file_index = Number(this.context_menu.attr('data-file-index'));
		if (isNaN(file_index))
			throw new Error("Invalid context menu data-file-index");
		const file_info = this.data[file_index];

		const dialog_prop: InputDialogProp = {
			message: "Enter new name of file",
			inputs: {
				name: {
					type: 'text',
					attr: { value: file_info.path }
				}
			}
		};

		const dialog = new InputDialog(dialog_prop);
		dialog.show();
		dialog.promise().then((response) => {
			if (response.type == DialogResponseType.submit)
				return this.move_specific(file_info.path, path.join(path.dirname(file_info.path), response.data.name as string)).then(() => { this.refresh(); });
		}).catch(show_error).then(() => { dialog.hide(); });
	}

	/**
	 * Remove file that have currently opened context menu.
	 * Show dialog and remove if user accept.
	 */
	context_menu_remove(): void {
		const file_index = Number(this.context_menu.attr('data-file-index'));
		if (isNaN(file_index))
			throw new Error("Invalid context menu data-file-index");
		const file_info = this.data[file_index];

		const dialog = new InputDialog({ message: "Do you want to remove '" + file_info.path + "'?", inputs: {} });
		dialog.show();
		dialog.promise().then((response) => {
			if (response.type == DialogResponseType.submit)
				return this.remove_specific(this.data[file_index].path).then(() => { this.refresh(); });
		}).catch(show_error).then(() => { dialog.hide(); });
	}

	get_data(): void { return; }
}
