import $ from 'jquery';
import { Dialog } from "./dialog_base";

/**
 * Avaliable type of HTML input element
 */
export type InputType = 'button' | 'checkbox' | 'color' | 'date' | 'datetime' | 'email' | 'file' | 'hidden' | 'password' | 'range' | 'search' | 'tel' | 'text' | 'time' | 'url';

/**
 * 
 */
export interface InputProp {
	/** Type of input element */
	type: InputType,
	/** Short description about this input element */
	tooltip?: string,
	/** HTML input attributes (for example value, required, ...) */
	attr?: {
		[key: string]: string
	}
}

export interface InputDialogProp {
	/** Headline message of input dialog */
	message: string,
	/** Object with all input elements to show, key is unique name */
	inputs: {
		[key: string]: InputProp
	},
	/** 
	 * Options to customize showed buttons and text.
	 * If value is string, button is enabled and have custom text.
	 * If value is true or undefined, button is enabled and have default text.
	 * If value is false, button is disabled and not visible.
	 */
	buttons?: {
		submit: boolean | string;
		cancel: boolean | string;
	}
}

/**
 * Output data from InputDialog.
 * Key of properties is equal to keys of `InputDialogProp.inputs`.
 */
export interface InputDialogOutput {
	[key: string]: string | number | string[] | undefined
}

export class InputDialog extends Dialog<InputDialogOutput> {
	static readonly dialog_layer: JQuery<HTMLElement> = $("#dialog_layer_input");

	readonly prop: InputDialogProp;

	private readonly description: JQuery;
	private readonly input_container: JQuery;
	private readonly button_container: JQuery;

	constructor(prop: InputDialogProp) {
		super($('#dialog_input'));

		this.prop = prop;

		this.description = this.element.find("#dialog_input_description");
		this.input_container = this.element.find("#dialog_input_input_container");
		this.button_container = this.element.find("#dialog_input_button_container");
	}

	// this dialog have own layer
	show(): void {
		this.description.text(this.prop.message);

		this.input_container.children().remove();
		Object.entries(this.prop.inputs).forEach((entry) => {
			this.generate_input_row(entry[1], entry[0]).appendTo(this.input_container);
		});

		this.button_container.children().remove();

		const submit_text = this.resolve_button_text('submit', 'Confirm');
		if (submit_text != null) {
			$('<button/>', {
				class: 'submit',
				text: submit_text
			}).appendTo(this.button_container);
		}

		const cancel_text = this.resolve_button_text('cancel', 'Cancel');
		if (cancel_text != null) {
			$('<button/>', {
				class: 'cancel',
				type: 'button',
				text: cancel_text
			}).appendTo(this.button_container);
		}

		InputDialog.dialog_layer.show();
	}

	/**
	 * Generate table row for specified input
	 * @param prop properties about input
	 * @param name unique name of input
	 * @returns JQuery <tr/> object
	 */
	private generate_input_row(prop: InputProp, name: string): JQuery {
		const row = $('<tr/>');

		if (prop.tooltip != null)
			$('<td/>').text(prop.tooltip).appendTo(row);

		const input = $('<input/>', {
			...prop.attr,
			type: prop.type,
			id: "dialog_input_" + name,
			'data-input-name': name
		});

		$('<td/>', {
			colspan: prop.tooltip == null ? 2 : 1
		}).append(input).appendTo(row);

		return row;
	}

	/**
	 * Response button text from this.prop
	 * @param type type of button
	 * @param default_text default button text if is not specified
	 * @returns button text or null if this button is disabled
	 */
	private resolve_button_text(type: 'submit' | 'cancel', default_text: string): string | null {
		if (this.prop.buttons == null)
			return default_text;
		const custom_text = this.prop.buttons[type];

		if (custom_text === true)
			return default_text;
		if (custom_text === false)
			return null;
		return custom_text;

	}

	hide(): void {
		InputDialog.dialog_layer.hide();
	}

	get_data(): InputDialogOutput {
		const output: InputDialogOutput = {};
		this.input_container.find('input').each((index, element) => {
			const name = $(element).attr('data-input-name');

			if (name != null)
				output[name] = $(element).val();
		});

		return output;
	}
}
