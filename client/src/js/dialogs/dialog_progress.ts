import $ from 'jquery';
import { Dialog } from './dialog_base';

export class ProgressDialog extends Dialog<void> {
	static readonly dialog_layer: JQuery<HTMLElement> = $("#dialog_layer_progress");

	static readonly UPDATE_INTERVAL = 50;

	private description: JQuery;
	private bar: JQuery;
	private percentage: JQuery;

	private interval: NodeJS.Timer | undefined;

	private description_text: string;
	private percentage_value: number;

	constructor(description: string) {
		super($('#dialog_progress'));

		this.description = this.element.find("#dialog_progress_description");
		this.bar = this.element.find("#dialog_progress_bar");
		this.percentage = this.element.find("#dialog_progress_percentage");

		this.description_text = description;
		this.percentage_value = 0;
	}

	// this dialog have own layer
	show(): void {
		this.description.text(this.description_text);
		this.update_progress(0);

		ProgressDialog.dialog_layer.show();
		this.draw();
		this.interval = setInterval(() => { this.draw(); }, ProgressDialog.UPDATE_INTERVAL);
	}

	hide(): void {
		if (this.interval != null) {
			clearInterval(this.interval);
			this.interval = undefined;
		}

		ProgressDialog.dialog_layer.hide();
	}

	update_progress(percentage: number): void {
		this.percentage_value = Math.min(Math.max(percentage, 0), 100);
	}

	draw(): void {
		const percentage_text = this.percentage_value.toFixed(2) + "%";
		this.bar.css("width", percentage_text);
		this.percentage.text(percentage_text);
	}

	get_data(): void { return; }
}
