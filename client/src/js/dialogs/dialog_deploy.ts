import $ from 'jquery';
import * as schema from 'pm2manager_schema';
import { ClientApi } from '../client_api';
import { DEPLOY_METHOD_TEXT, show_error } from '../utils';
import { Dialog } from './dialog_base';

export class DeployDialog extends Dialog<schema.RequestSetDeployData> {
	readonly process_id: number;

	readonly method_select: JQuery;
	readonly branch_input: JQuery;
	readonly secret_view: JQuery;
	readonly use_ssh_checkbox: JQuery;
	readonly ssh_key_view: JQuery;
	readonly build_stage_input: JQuery;

	readonly reset_secret_button: JQuery;
	readonly reset_key_button: JQuery;
	readonly build_button: JQuery;

	readonly api: ClientApi;

	private original_data?: schema.ResponseDeployData;

	constructor(process_id: number, api: ClientApi) {
		super($("#dialog_deploy"));
		this.process_id = process_id;
		this.api = api;

		this.method_select = this.element.find("#dialog_deploy_method");
		this.init_method_select();

		this.branch_input = this.element.find("#dialog_deploy_branch");
		this.secret_view = this.element.find("#dialog_deploy_secret");
		this.use_ssh_checkbox = this.element.find("#dialog_deploy_ssh");
		this.ssh_key_view = this.element.find("#dialog_deploy_key");
		this.build_stage_input = this.element.find("#dialog_deploy_build_stage");

		this.reset_secret_button = this.element.find("#dialog_deploy_reset_secret");
		this.reset_key_button = this.element.find("#dialog_deploy_reset_key");
		this.build_button = this.element.find("#dialog_deploy_build");
	}

	init_method_select(): void {
		this.method_select.children().remove();
		for (const key_str in Object.keys(DEPLOY_METHOD_TEXT)) {
			const key = Number(key_str);
			if (isNaN(key))
				continue;

			this.method_select.append($("<option/>", {
				val: key_str,
				text: DEPLOY_METHOD_TEXT[key as keyof typeof DEPLOY_METHOD_TEXT]
			}));
		}
	}

	show(): void {
		super.show();

		this.reset_secret_button.on('click', () => {
			this.reset_secret();
		});

		this.reset_key_button.on('click', () => {
			this.reset_ssh_key();
		});

		this.build_button.on('click', () => {
			this.build();
		});

		this.method_select.on('change', () => {
			if (this.method_select.val() != schema.DeployMethod.none && this.secret_view.text().trim() == "")
				this.reset_secret();
		});

		this.use_ssh_checkbox.on('change', () => {
			if (this.use_ssh_checkbox.prop('checked') == true && this.ssh_key_view.text().trim() == "")
				this.reset_ssh_key();
		});
	}

	hide(): void {
		super.hide();

		this.secret_view.text("");
		this.reset_secret_button.off('click');
		this.reset_key_button.off('click');
		this.build_button.off('click');

		this.method_select.off('change');
		this.use_ssh_checkbox.off('change');
	}

	set_data(data: schema.ResponseDeployData): void {
		this.original_data = data;

		this.method_select.val(data.method);
		this.branch_input.val(data.branch ?? "");
		this.secret_view.text(data.secret ?? "");
		this.use_ssh_checkbox.prop("checked", data.use_ssh);
		this.ssh_key_view.text(data.ssh_key ?? "");
		this.build_stage_input.val(data.build_stage ?? "");
	}

	reset_secret(): void {
		this.api.reset_deploy_secret(this.process_id).then((response) => {
			this.secret_view.text(response.secret);
		}).catch(show_error);
	}

	reset_ssh_key(): void {
		this.api.reset_deploy_key(this.process_id).then((response) => {
			this.ssh_key_view.text(response.ssh_key);
		}).catch(show_error);
	}

	build(): void {
		if (this.original_data == null)
			throw new Error("Trying to build without saved original data");

		this.api.set_deploy(this.process_id, undefined, undefined, undefined, this.build_stage_input.val()?.toString()).then(() => {
			return this.api.build(this.process_id);
		}).catch(show_error);
	}

	get_data(): schema.RequestSetDeployData {
		return {
			process_id: this.process_id,
			method: Number(this.method_select.val()),
			branch: this.branch_input.val()?.toString(),
			use_ssh: this.use_ssh_checkbox.prop("checked"),
			build_stage: this.build_stage_input.val()?.toString()
		};
	}
}
