import $ from 'jquery';
import * as schema from 'pm2manager_schema';
import { ClientApi } from '../client_api';
import { show_error } from '../utils';
import { Dialog } from "./dialog_base";

interface LogChunkData {
	start: number;
	buffer: Buffer;
}

type LogDialogDataDirtyCallback = (top: boolean, bottom: boolean) => void;
type LogDialogDataWatchCallback = (old_length: number, new_length: number) => void;

class LogDialogData {
	readonly process_id: number;
	fd: number | undefined;

	/**
	 * Buffer data of visible chunk of log
	 */
	data: LogChunkData | undefined;

	/** 
	 * Maximum length of stored log in memory.
	 * If `main_data` is larger then this value, top or bottom will be cropped based of position of new received data
	 */
	readonly max_length: number;

	/**
	 * Start of file. True if start of log is currently loaded.
	 */
	get sof(): boolean {
		return this.data != null && this.data.start == 0;
	}

	/**
	 * End of file. True if end of log is currently loaded.
	 */
	get eof(): boolean {
		return this.data != null && this.data.start + this.data.buffer.byteLength >= this.log_length;
	}

	/** byte position of end of file */
	log_length: number;

	/**
	 * Callback that is called if new data is appended.
	 * Each boolean in parameter represent if top or bottom of log is changed.
	 */
	dirty_callback: LogDialogDataDirtyCallback;
	/**
	 * Callback that is called if log size in server changed.
	 */
	watch_callback: LogDialogDataWatchCallback;

	/**
	 * True if some request to api is called and waiting to response, or this object is not initialized.
	 */
	private request_in_progress: boolean;

	constructor(process_id: number, max_length: number, dirty_callback: LogDialogDataDirtyCallback, watch_callback: LogDialogDataWatchCallback) {
		this.process_id = process_id;
		this.max_length = max_length;

		this.dirty_callback = dirty_callback;
		this.watch_callback = watch_callback;

		this.log_length = 0;
		this.request_in_progress = true; // object is not initialized and cannot process requests
	}

	/**
	 * Initialize object. This function must be called before any requests.
	 * Open a log file and start watching.
	 * @param log_type type of log
	 * @param start_length length of initial read request
	 */
	async init(api: ClientApi, log_type: schema.LogType, start_length: number): Promise<void> {
		const open_data = await api.open_log(this.process_id, log_type);
		this.fd = open_data.fd;
		this.log_length = open_data.size;

		await api.watch_file(open_data.process_id, open_data.fd, (data) => {
			if (data.event_type == 'change') {
				if (data.size == null) {
					show_error("Watched log file has been removed");
					api.unwatch_file(open_data.process_id, open_data.fd).catch(show_error);
					return;
				}

				const old_length = this.log_length;
				this.log_length = data.size;
				this.watch_callback(old_length, this.log_length);
			}
		});

		this.request_in_progress = false;
		await this.request_top(api, start_length);
	}

	/**
	 * Close opened log and free used memory.
	 * @param api 
	 */
	async destroy(api: ClientApi): Promise<void> {
		this.request_in_progress = true;
		if (this.fd != null)
			await api.close_file(this.process_id, this.fd);

		this.fd = undefined;
		this.data = undefined;
		this.log_length = 0;
	}

	/**
	 * Write received buffers to this.data and call dirty callback.
	 * @param data array of received buffers, all must be from one side (begin or end) and sorted
	 */
	private put_data(data: LogChunkData[]): void {
		if (data.length <= 0)
			return;

		// merge buffers
		const isBegin = data[0].start < (this.data != null ? this.data.start : -1);
		console.log("put_data received: ");
		console.log(data);
		console.log(this.data);

		if (this.data != null) {
			if (isBegin) {
				if (data[data.length - 1].start + data[data.length - 1].buffer.byteLength != this.data.start)
					throw new Error("Cannot merge buffers with hole");
				data.push(this.data);
			} else {
				if (data[0].start != this.data.start + this.data.buffer.byteLength)
					throw new Error("Cannot merge buffers with hole");
				data.splice(0, 0, this.data);
			}
		}

		let new_buffer = Buffer.concat(data.map((value) => value.buffer));
		let new_start = data[0].start;

		let dirty_top = false;
		let dirty_bottom = false;

		if (isBegin)
			dirty_top = true;
		else
			dirty_bottom = true;

		// crop to max_length
		const old_size = new_buffer.byteLength;
		if (old_size > this.max_length) {
			if (isBegin) {
				new_buffer = new_buffer.subarray(0, this.max_length);
				dirty_bottom = true;
			}
			else {
				const offset = this.max_length - old_size;
				new_buffer = new_buffer.subarray(offset, offset + this.max_length);
				new_start += offset;
				dirty_top = true;
			}
		}

		this.data = {
			buffer: new_buffer,
			start: new_start
		};

		this.dirty_callback(dirty_top, dirty_bottom);
	}

	/**
	 * Request a new chunk of log to extend begin of data
	 * @param length length of new chunk
	 */
	request_top(api: ClientApi, length: number): Promise<void> {
		if (this.request_in_progress) {
			console.warn("skipped request_top with length " + length);
			return Promise.resolve();
		}

		// compute start byte offset and crop to non-negative number
		let start = (this.data != null ? this.data.start : this.log_length) - length;
		length = Math.min(length + Math.min(start, 0), this.max_length);
		start = Math.max(start, 0);

		return this.request_api(api, start, length);
	}

	/**
	 * Request a new chunk of log to extend end of data
	 * @param length length of new chunk
	 */
	request_bottom(api: ClientApi, length: number): Promise<void> {
		if (this.request_in_progress) {
			console.warn("skipped request_bottom with length " + length);
			return Promise.resolve();
		}

		if (this.data == null)
			return this.request_top(api, length);
		let start = this.data.start + this.data.buffer.byteLength;
		length -= Math.max(start + length - this.log_length, 0);

		// crop request to max_length
		const end = start + length;
		length = Math.min(length, this.max_length);
		start = end - length;

		return this.request_api(api, start, length);
	}

	/**
	 * Request a new chunk of log.
	 * This function is not for calling explicitly, but via request_top/request_bottom functions.
	 * @param start byte offset from begining of log file
	 * @param length length of new chunk
	 */
	private async request_api(api: ClientApi, start: number, length: number): Promise<void> {
		if (this.fd == null)
			throw new Error("Requesting data before init");

		this.request_in_progress = true;

		const chunks: LogChunkData[] = [];
		await api.read_file_chunk(this.process_id, this.fd, start, length, (chunk) => {
			chunks.push({ buffer: chunk.buffer, start: chunk.start });
		});

		this.request_in_progress = false;
		this.put_data(chunks);
	}

	/**
	 * Reset stored log data and request new starting chunk
	 * @param start_length length of starting chunk
	 * @returns 
	 */
	reset(api: ClientApi, start_length: number): Promise<void> {
		this.data = undefined;
		return this.request_top(api, start_length);
	}

	toString(): string {
		if (this.data)
			return this.data.buffer.toString('utf8');
		return "";
	}
}

interface ScrollInfo {
	top: number,
	bottom: number
}

export class LogDialog extends Dialog<void> {
	static readonly SCROLL_THRESHOLD = 25;
	/**
	 * Size of single chunk request
	 */
	static readonly CHUNK_LENGTH = 8 * 1024;
	/**
	 * Maximum size of stored log in memory.
	 */
	static readonly MAX_LOGDATA_LENGTH = 1024 * 1024;

	readonly process_id: number;

	private readonly buttons: { [key in schema.LogType]: JQuery };
	private readonly container: JQuery;
	private readonly view: JQuery;

	private ignore_scroll: boolean;

	private selected: schema.LogType;
	private data: { [key in schema.LogType]: LogDialogData };

	readonly api: ClientApi;

	constructor(process_id: number, api: ClientApi) {
		super($("#dialog_log"));

		this.process_id = process_id;

		this.buttons = {
			stdout: this.element.find("#dialog_log_stdout"),
			stderr: this.element.find("#dialog_log_stderr"),
			pm2m: this.element.find("#dialog_log_pm2m")
		};

		this.selected = 'stdout';
		this.container = this.element.find("#dialog_log_container");
		this.view = this.element.find("#dialog_log_view");

		this.ignore_scroll = false;

		this.data = {
			stdout: this.prepare_log_data('stdout'),
			stderr: this.prepare_log_data('stderr'),
			pm2m: this.prepare_log_data('pm2m')
		};

		this.api = api;
	}

	private prepare_log_data(type: schema.LogType): LogDialogData {
		return new LogDialogData(this.process_id, LogDialog.MAX_LOGDATA_LENGTH, (top, bottom) => {
			this.dirty_handler(type, top, bottom);
		}, () => {
			this.watch_handler(type);
		});
	}

	get_data(): void { return; }

	async show(): Promise<void> {
		const promises = [
			this.data.stdout.init(this.api, 'stdout', LogDialog.CHUNK_LENGTH),
			this.data.stderr.init(this.api, 'stderr', LogDialog.CHUNK_LENGTH),
			this.data.pm2m.init(this.api, 'pm2m', LogDialog.CHUNK_LENGTH)
		];
		await Promise.all(promises);

		super.show();

		Object.entries(this.buttons).forEach(([key, element]) => {
			element.on('click', () => {
				this.select(key as schema.LogType);
			});
		});

		this.container.on('scroll', () => { this.check_scroll(); });
		this.container.on('resize', () => { this.check_scroll(); });
	}

	hide(): void {
		super.hide();

		this.data.stdout.destroy(this.api);
		this.data.stderr.destroy(this.api);
		this.data.pm2m.destroy(this.api);

		Object.values(this.buttons).forEach((element) => element.off('click'));
		this.container.off('scroll');
		this.container.off('resize');
		this.view.text("");
	}

	/** 
	 * Switch selected log in view
	 */
	select(type: schema.LogType): void {
		this.selected = type;
		this.data[this.selected].reset(this.api, LogDialog.CHUNK_LENGTH).catch(show_error);
	}

	/**
	 * Get current scroll offset from top and bottom of container
	 */
	get_scroll(): ScrollInfo {
		const scroll_top = this.container.scrollTop() ?? 0;
		const view_height = this.view.outerHeight() ?? 0;
		const height = this.container.innerHeight() ?? 0;
		const scroll_bottom = view_height - (scroll_top + height);
		return { top: scroll_top, bottom: scroll_bottom };
	}

	/**
	 * Set scroll offset from bottom
	 */
	set_scroll_bottom(scroll_bottom: number): void {
		const view_height = this.view.outerHeight() ?? 0;
		const height = this.container.innerHeight() ?? 0;
		const scroll_top = view_height - (scroll_bottom + height);
		this.container.scrollTop(scroll_top);
	}

	/**
	 * Callback dirty handler from LogDialogData.
	 * Redraw log view if is needed.
	 */
	private dirty_handler(log_type: schema.LogType, dirty_top: boolean, dirty_bottom: boolean): void {
		console.log("dirty_handler from " + log_type);
		if (this.selected == log_type)
			this.redraw(dirty_top, dirty_bottom);
	}

	/**
	 * Callback watch handler from LogDialogData.
	 */
	private watch_handler(log_type: schema.LogType): void {
		if (this.selected == log_type)
			this.check_scroll();
	}

	/**
	 * Check if user scroll to begin or end of showed log and another chunk must be requested.
	 */
	check_scroll(): void {
		if (this.ignore_scroll) {
			this.ignore_scroll = false;
			console.log("ignoring scroll!");
			return;
		}

		const scroll = this.get_scroll();

		if (scroll.top < LogDialog.SCROLL_THRESHOLD && !this.data[this.selected].sof)
			this.request_top();
		else if (scroll.bottom < LogDialog.SCROLL_THRESHOLD && !this.data[this.selected].eof)
			this.request_bottom();
	}

	stay_bottom(old_scroll: ScrollInfo): void {
		const current_scroll = this.get_scroll();
		const new_scroll = current_scroll.top + (current_scroll.bottom - old_scroll.bottom);
		console.log("stay_bottom: " + new_scroll + " != " + current_scroll.top);
		if (new_scroll != current_scroll.top) {
			//this.ignore_scroll = true; FIXME: test it
			this.container.scrollTop(new_scroll);
		}
	}

	/**
	 * Request a new chunk of log to extend begin of data
	 */
	request_top(): void {
		this.data[this.selected].request_top(this.api, LogDialog.CHUNK_LENGTH);
	}

	/**
	 * Request a new chunk of log to extend end of data
	 */
	request_bottom(): void {
		this.data[this.selected].request_bottom(this.api, LogDialog.CHUNK_LENGTH);
	}

	/**
	 * Redraw log view
	 * @param dirty_top true if begin of data was changed
	 * @param dirty_bottom true if end of data was changed
	 */
	redraw(dirty_top: boolean, dirty_bottom: boolean): void {
		const log_data = this.data[this.selected];
		const old_scroll = this.get_scroll();
		this.view.text(log_data.toString());
		if ((dirty_top && !dirty_bottom) || (dirty_bottom && old_scroll.bottom < LogDialog.SCROLL_THRESHOLD))
			this.stay_bottom(old_scroll);

		const new_scroll = this.get_scroll();
		console.log(new_scroll);
		if (log_data.data != null && new_scroll.top <= 0 && new_scroll.bottom <= 0) {
			console.log("requesting more from draw");
			if (log_data.data.start == 0) {
				if (!log_data.eof)
					this.request_bottom();
			}
			else
				this.request_top();
		}
	}
}
