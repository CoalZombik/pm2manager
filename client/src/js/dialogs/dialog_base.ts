import $ from 'jquery';

export enum DialogResponseType {
	submit = 'submit',
	cancel = 'cancel'
}

export interface DialogResponse<T> {
	type: DialogResponseType;
	data: T;
}

export abstract class Dialog<T> {
	static readonly dialog_layer = $("#dialog_layer");

	element: JQuery;

	constructor(element: JQuery) {
		this.element = element;
	}

	show(): void | Promise<void> {
		Dialog.dialog_layer.show();
		Dialog.dialog_layer.children().hide();
		this.element.show();
	}

	hide(): void {
		Dialog.dialog_layer.hide();
	}

	abstract get_data(): T;

	promise(): Promise<DialogResponse<T>> {
		return new Promise((resolve) => {
			const callback = (e: { preventDefault(): void }, type: DialogResponseType): void => {
				e.preventDefault();
				for (const type in DialogResponseType) {
					this.element.find("button." + type).off('click');
				}
				this.element.off('submit');

				const data = {
					type: type,
					data: this.get_data()
				};

				resolve(data);
			};

			for (const type in DialogResponseType) {
				const button = this.element.find("button." + type);
				button.on('click', (e) => { callback(e, type as DialogResponseType); });
			}
			this.element.on('submit', (e) => { callback(e, DialogResponseType.submit); });
		});
	}
}
