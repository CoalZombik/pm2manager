import $ from 'jquery';
import * as schema from 'pm2manager_schema';
import { Dialog } from "./dialog_base";

export class LoginDialog extends Dialog<schema.RequestAuthPasswordData> {
	private username: JQuery;
	private password: JQuery;
	private error: JQuery;

	constructor() {
		super($('#dialog_login'));

		this.username = this.element.find("#dialog_login_username");
		this.password = this.element.find("#dialog_login_password");
		this.error = this.element.find("#dialog_login_error");
	}

	show(): void {
		this.error.hide();
		super.show();
	}

	hide(): void {
		super.hide();
		this.password.val("");
	}

	get_data(): schema.RequestAuthPasswordData {
		return {
			username: (this.username.val()?.toString() ?? ""),
			password: (this.password.val()?.toString() ?? "")
		};
	}

	set_error(message: string): void {
		this.error.text(message);
		this.error.show();
	}
}