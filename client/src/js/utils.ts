import * as schema from "pm2manager_schema";

export function timestamp_to_time(time: number): string {
	let prefix = "";
	if (time < 0) {
		prefix = "-";
		time = -time;
	}

	const seconds = Math.round(time % 60);
	const minutes = Math.floor(time / 60) % 60;
	const hours = Math.floor(time / 3600);

	const format_option = {
		minimumIntegerDigits: 2,
		useGrouping: false
	};

	return prefix + hours.toLocaleString(undefined, format_option) + ":" + minutes.toLocaleString(undefined, format_option) + ":" + seconds.toLocaleString(undefined, format_option);
}

export function format_memory(memory: number): string {
	if (memory > 512 * 1024 * 1024)
		return (memory / (1024 * 1024 * 1024)).toFixed(1) + " GiB";
	if (memory > 512 * 1024)
		return (memory / (1024 * 1024)).toFixed(1) + " MiB";
	if (memory > 512)
		return (memory / 1024).toFixed(1) + " KiB";
	return memory + " B";
}

/**
 * @returns Random string id
 */
export function random_id(): string {
	return Math.random().toString(36).substring(2);
}

/**
 * @returns Same as random_id, but with checking if any dom object already exists with this id
 */
export function random_id_unique(): string {
	let output_id;
	do {
		output_id = random_id();
	} while ($("#" + output_id).length != 0);
	return output_id;
}

/**
 * Get element id and generate new if not exists
 * @param default_id new element id for case if element doesn't have one (or generate unique if is undefined)
 * @returns element id
 */
export function force_dom_id(element: JQuery, default_id?: string): string {
	let id = element.attr('id');
	if (id == null) {
		id = default_id != null ? default_id : random_id_unique();
		element.attr('id', id);
	}
	return id;
}

export class TimedInterval {
	timeout: NodeJS.Timeout | null;
	interval: NodeJS.Timeout | null;

	constructor(callback: () => void, interval: number, start: number | { modulo: number, offset: number }, first_immediately?: boolean) {
		if (first_immediately === true)
			callback();

		const timeout_time = typeof start === 'number' ? (Date.now() - start) : (start.modulo - (Date.now() % start.modulo) + start.offset);
		this.timeout = setTimeout(() => {
			callback();
			this.interval = setInterval(callback, interval);
		}, timeout_time);
		this.interval = null;
	}

	is_active(): boolean {
		return this.timeout != null || this.interval != null;
	}

	clear(): void {
		if (this.timeout) {
			clearTimeout(this.timeout);
			this.timeout = null;
		}

		if (this.interval) {
			clearInterval(this.interval);
			this.interval = null;
		}
	}
}

export function show_error(message: string): void {
	alert(message); //TODO: Better
}

export function promise_sleep(time: number): Promise<void> {
	return new Promise((resolve) => {
		setTimeout(resolve, time);
	});
}

export const PROC_STATUS_TEXT: { [key in schema.ProcStatus]: string } = {
	[schema.ProcStatus.unknown]: 'unknown',
	[schema.ProcStatus.online]: 'online',
	[schema.ProcStatus.stopping]: 'stopping',
	[schema.ProcStatus.stopped]: 'stopped',
	[schema.ProcStatus.launching]: 'launching',
	[schema.ProcStatus.errored]: 'errored',
	[schema.ProcStatus.one_launch_status]: 'one launch',
	[schema.ProcStatus.not_spawned]: 'not spawned',
	[schema.ProcStatus.updating]: 'updating',
	[schema.ProcStatus.custom]: 'custom'
};

export const DEPLOY_METHOD_TEXT: { [key in schema.DeployMethod]: string } = {
	[schema.DeployMethod.none]: 'none',
	[schema.DeployMethod.github]: 'github webhook',
	[schema.DeployMethod.api]: 'pm2manager rest api'
};

export const PATH_TYPE_TEXT: { [key in schema.PathType]: string } = {
	[schema.PathType.unknown]: "unknown",
	[schema.PathType.fifo]: 'fifo',
	[schema.PathType.character_device]: 'character device',
	[schema.PathType.directory]: 'directory',
	[schema.PathType.block_device]: 'block device',
	[schema.PathType.regular_file]: 'file',
	[schema.PathType.symbolic_link]: 'symlink',
	[schema.PathType.socket]: 'socket',
};
