import $ from 'jquery';
import Cookies from 'js-cookie';
import * as schema from 'pm2manager_schema';
import { ClientApi } from './client_api';
import * as Dialog from './dialogs';
import { HtmlProcess } from './html_process';
import { show_error } from './utils';

const NON_ACTIVE_THRESHOLD = 1500;
const API_URL = process.env.WS_URL != null ? process.env.WS_URL : "ws://localhost";

const api: ClientApi = new ClientApi(show_error);

const process_list_container = $("#process_list");
const process_list: Map<number, HtmlProcess> = new Map();

let update_interval: NodeJS.Timeout | undefined = undefined;

async function login(): Promise<void> {
	let token = Cookies.get("pm2-token");
	let login_data: schema.ResponseAuthData | null = null;

	if (token != null) {
		try {
			login_data = await api.auth_by_token(token);
		} catch (e) {
			console.error(e);
			token = undefined;
		}
	}

	if (token == null) {
		const dialog = new Dialog.Login();
		dialog.show();

		let valid_token = false;
		while (!valid_token) {
			try {
				const response = await dialog.promise();
				login_data = await api.auth_by_login(response.data.username, response.data.password);
				valid_token = true;
			} catch (e) {
				dialog.set_error(String(e));
			}
		}

		dialog.hide();

		if (login_data?.token != null && login_data.valid_until != null) {
			Cookies.set("pm2-token", login_data.token, {
				expires: new Date(login_data.valid_until * 1000),
				path: '',
				sameSite: 'strict'
			});
		}
	}


	if (login_data)
		$("#user_name").text(login_data.username);
	$("#user_logout").on('click', () => { logout(); });
}

function logout(): void {
	Cookies.remove('pm2-token', { path: '' });
	location.reload();
}

function start_update_interval(): void {
	update_interval = setInterval(() => {
		api.get_process_list().then(update_process_list).catch(show_error);
	}, 1000);
}

function stop_update_interval(): void {
	if (update_interval)
		clearInterval(update_interval);
}

function update_process_list(data: schema.ResponseProcessListData): void {
	data.info.forEach(element => {
		let process = process_list.get(element.db_id);
		if (!process) {
			process = HtmlProcess.spawn(process_list_container, element.db_id, (process_id, start, end) => api.get_process_monit(process_id, start, end));

			process.on_click('start', start_process);
			process.on_click('stop', stop_process);
			process.on_click('deploy', open_deploy_process);
			process.on_click('env', open_env_process);
			process.on_click('logs', open_log_process);
			process.on_click('files', open_files_process);

			process_list.set(element.db_id, process);
		}

		process.update(element);
	});

	//clear all non-active processes
	const update_time = Date.now();
	process_list.forEach((element, key) => {
		if (element.last_update < update_time - NON_ACTIVE_THRESHOLD) {
			element.delete();
			process_list.delete(key);
		}
	});
}

function start_process(process_id: number): void {
	api.start_process(process_id).then((data) => { console.log("Process " + data.process_id + " started"); }).catch(show_error);
}

function stop_process(process_id: number): void {
	api.stop_process(process_id).then((data) => { console.log("Process " + data.process_id + " stopped"); }).catch(show_error);
}

function open_deploy_process(process_id: number): void {
	const dialog = new Dialog.Deploy(process_id, api);

	api.get_deploy(process_id).then((data) => {
		dialog.show();
		dialog.set_data(data);
		return dialog.promise();
	}).then((response) => {
		if (response.type == Dialog.ResponseType.submit)
			return api.set_deploy(process_id, response.data.method, response.data.branch, response.data.use_ssh);
	}).catch(show_error).then(() => { dialog.hide(); });
}

function open_env_process(process_id: number): void {
	const dialog = new Dialog.Env();

	api.get_env(process_id).then((data) => {
		dialog.show();
		dialog.set_data(data.env);
		return dialog.promise();
	}).then((dialog_response) => {
		if (dialog_response.type == Dialog.ResponseType.submit)
			return api.set_env(process_id, dialog_response.data);
	}).catch(show_error).then(() => { dialog.hide(); });
}

function open_log_process(process_id: number): void {
	const dialog = new Dialog.Log(process_id, api);
	dialog.show().then(() => dialog.promise()).catch(show_error).then(() => { dialog.hide(); });
}

function open_files_process(process_id: number): void {
	const dialog = new Dialog.Files(process_id, api);
	dialog.show();
	dialog.promise().catch(show_error).then(() => { dialog.hide(); });
}

//Start
$(() => {
	api.open(API_URL).then(() => login()).then(start_update_interval);
});
