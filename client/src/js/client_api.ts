import * as schema from 'pm2manager_schema';

interface ApiEvent<T extends schema.Response['data']> {
	resolve: (data: T) => void;
	reject: (message: string) => void;
	only_once?: boolean;
	request_id?: number;
}

type ApiFunctionSet = {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key in Exclude<schema.RequestType, 'error'>]: (...params: any) => (schema.ResponseData | void) | Promise<schema.ResponseData | void>;
};

export type FileChunk = Omit<schema.ResponseReadFileChunk['data'], 'buffer'> & { buffer: Buffer };

export class ClientApi implements ApiFunctionSet {
	private ws?: WebSocket;
	error_callback: (message: string) => void;

	private next_request_id: number;

	private events: Map<schema.RequestType, ApiEvent<schema.Response['data']>[]>;

	constructor(error_callback: (message: string) => void) {
		this.error_callback = error_callback;
		this.next_request_id = 0;
		this.events = new Map();
	}

	open(url: string): Promise<void> {
		return new Promise((resolve) => {
			this.ws = new global.WebSocket(url);
			this.ws.addEventListener('open', () => { resolve(); });
			this.ws.addEventListener('error', (e) => { this.error_callback((e as CloseEvent).reason); });
			this.ws.addEventListener('message', (event_data) => { this.parse_message(event_data.data); });
		});
	}

	close(): void {
		this.ws?.close();
	}

	private get_events<T extends schema.Response>(type: T['type']): ApiEvent<T['data']>[] {
		if (!this.events.has(type))
			this.events.set(type, []);
		return this.events.get(type) as ApiEvent<T['data']>[];
	}

	on<T extends schema.Response>(type: T['type'], resolve: (data: T['data']) => void, reject: (message: string) => void, request_id?: number): void {
		this.get_events(type).push({ resolve: resolve, reject: reject, request_id: request_id });
	}

	once<T extends schema.Response>(type: T['type'], resolve: (data: T['data']) => void, reject: (message: string) => void, request_id?: number): void {
		this.get_events(type).push({ resolve: resolve, reject: reject, request_id: request_id, only_once: true });
	}

	off<T extends schema.Response>(type: T['type'], request_id?: number): void {
		const event_array = this.get_events(type);
		const index = event_array.findIndex((element) => element.request_id == request_id);
		if (index >= 0)
			event_array.splice(index, 1);
	}

	private prepare_request<T extends schema.Request>(request: T): T & { request_id: number } {
		return Object.assign({ request_id: this.next_request_id++ }, request);
	}

	private send<T extends schema.Request>(request: T): void {
		if (!this.ws || this.ws.readyState != WebSocket.OPEN)
			throw new Error("API is not prepared to send data!");

		this.ws.send(JSON.stringify(request));
	}

	// Probably wrong usage of generics, but offer at least some type check
	private send_promise<T extends schema.Request, Q extends schema.Response>(request: T): Promise<Q['data']> {
		return new Promise((resolve, reject) => {
			this.once<Q>(request.type, resolve, reject, request.request_id);
			this.send(request);
		});
	}

	private parse_message(message: string): void {
		const response: schema.Response = JSON.parse(message);

		if (response.type == null) { // TODO: Better checking and error handling
			alert("Invalid request_type");
		}
		else {
			const event_array = this.get_events(response.type);

			for (let i = 0; i < event_array.length; i++) {
				const element = event_array[i];
				if (element.request_id != undefined && element.request_id != response.request_id)
					continue;

				if (response.status == 200)
					element.resolve(response.data);
				else
					element.reject((response.data as schema.ResponseErrorData).reason);

				if (element.only_once === true) { // Remove once type event
					event_array.splice(i, 1);
					i--;
				}
			}
		}
	}

	/**
	 * Clear all events with specific type and request_id
	 */
	private clear_events(type: schema.RequestType, request_id: number): void {
		const event_array = this.get_events(type);
		for (let i = 0; i < event_array.length; i++) {
			const element = event_array[i];
			if (element.request_id != request_id)
				continue;

			//Remove
			event_array.splice(i, 1);
			i--;
		}
	}

	auth(data: schema.RequestAuthData): Promise<schema.ResponseAuth['data']> {
		const request = this.prepare_request<schema.RequestAuth>({ type: 'auth', data: data });
		return this.send_promise<schema.RequestAuth, schema.ResponseAuth>(request);
	}

	auth_by_token(token: string): Promise<schema.ResponseAuth['data']> {
		const request = this.prepare_request<schema.RequestAuth>({ type: 'auth', data: { token: token } });
		return this.send_promise<schema.RequestAuth, schema.ResponseAuth>(request);
	}

	auth_by_login(username: string, password: string): Promise<schema.ResponseAuth['data']> {
		const request = this.prepare_request<schema.RequestAuth>({ type: 'auth', data: { username: username, password: password } });
		return this.send_promise<schema.RequestAuth, schema.ResponseAuth>(request);
	}

	get_process_list(): Promise<schema.ResponseGetProcessList['data']> {
		const request = this.prepare_request<schema.RequestGetProcessList>({ type: 'get_process_list', data: null });
		return this.send_promise<schema.RequestGetProcessList, schema.ResponseGetProcessList>(request);
	}

	get_process_info(process_id: number): Promise<schema.ResponseGetProcessInfo['data']> {
		const request = this.prepare_request<schema.RequestGetProcessInfo>({ type: 'get_process_info', data: { process_id: process_id } });
		return this.send_promise<schema.RequestGetProcessInfo, schema.ResponseGetProcessInfo>(request);
	}

	get_process_monit(process_id: number, start: number, end: number): Promise<schema.ResponseGetProcessMonit['data']> {
		const request = this.prepare_request<schema.RequestGetProcessMonit>({ type: 'get_process_monit', data: { process_id: process_id, start: start, end: end } });
		return this.send_promise<schema.RequestGetProcessMonit, schema.ResponseGetProcessMonit>(request);
	}

	open_log(process_id: number, log_type: schema.LogType): Promise<schema.ResponseOpenLog['data']> {
		const request = this.prepare_request<schema.RequestOpenLog>({ type: 'open_log', data: { process_id: process_id, log_type: log_type } });
		return this.send_promise<schema.RequestOpenLog, schema.ResponseOpenLog>(request);
	}

	start_process(process_id: number): Promise<schema.ResponseStartProcess['data']> {
		const request = this.prepare_request<schema.RequestStartProcess>({ type: 'start_process', data: { process_id: process_id } });
		return this.send_promise<schema.RequestStartProcess, schema.ResponseStartProcess>(request);
	}

	stop_process(process_id: number): Promise<schema.ResponseStopProcess['data']> {
		const request = this.prepare_request<schema.RequestStopProcess>({ type: 'stop_process', data: { process_id: process_id } });
		return this.send_promise<schema.RequestStopProcess, schema.ResponseStopProcess>(request);
	}

	get_env(process_id: number): Promise<schema.ResponseGetEnv['data']> {
		const request = this.prepare_request<schema.RequestGetEnv>({ type: 'get_env', data: { process_id: process_id } });
		return this.send_promise<schema.RequestGetEnv, schema.ResponseGetEnv>(request);
	}

	set_env(process_id: number, env: { [key: string]: string }): Promise<schema.ResponseSetEnv['data']> {
		const request = this.prepare_request<schema.RequestSetEnv>({ type: 'set_env', data: { process_id: process_id, env: env } });
		return this.send_promise<schema.RequestSetEnv, schema.ResponseSetEnv>(request);
	}

	get_deploy(process_id: number): Promise<schema.ResponseGetDeploy['data']> {
		const request = this.prepare_request<schema.RequestGetDeploy>({ type: 'get_deploy', data: { process_id: process_id } });
		return this.send_promise<schema.RequestGetDeploy, schema.ResponseGetDeploy>(request);
	}

	set_deploy(process_id: number, deploy_method?: schema.DeployMethod, branch?: string, use_ssh?: boolean, build_stage?: string | null): Promise<schema.ResponseSetDeploy['data']> {
		const request = this.prepare_request<schema.RequestSetDeploy>({
			type: 'set_deploy', data: {
				process_id: process_id, method: deploy_method, branch: branch, use_ssh: use_ssh, build_stage: build_stage
			}
		});
		return this.send_promise<schema.RequestSetDeploy, schema.ResponseSetDeploy>(request);
	}

	build(process_id: number): Promise<schema.ResponseBuild['data']> {
		const request = this.prepare_request<schema.RequestBuild>({ type: 'build', data: { process_id: process_id } });
		return this.send_promise<schema.RequestBuild, schema.ResponseBuild>(request);
	}

	reset_deploy_secret(process_id: number): Promise<schema.ResponseResetDeploySecret['data']> {
		const request = this.prepare_request<schema.RequestResetDeploySecret>({ type: 'reset_deploy_secret', data: { process_id: process_id } });
		return this.send_promise<schema.RequestResetDeploySecret, schema.ResponseResetDeploySecret>(request);
	}

	reset_deploy_key(process_id: number): Promise<schema.ResponseResetDeployKey['data']> {
		const request = this.prepare_request<schema.RequestResetDeployKey>({ type: 'reset_deploy_key', data: { process_id: process_id } });
		return this.send_promise<schema.RequestResetDeployKey, schema.ResponseResetDeployKey>(request);
	}

	list_directory(process_id: number, path: string): Promise<schema.ResponseListDirectory['data']> {
		const request = this.prepare_request<schema.RequestListDirectory>({
			type: 'list_directory', data: {
				process_id: process_id, path: path
			}
		});
		return this.send_promise<schema.RequestListDirectory, schema.ResponseListDirectory>(request);
	}

	create_directory(process_id: number, path: string, recursive: boolean): Promise<schema.ResponseCreateDirectory['data']> {
		const request = this.prepare_request<schema.RequestCreateDirectory>({
			type: 'create_directory', data: {
				process_id: process_id, path: path, recursive: recursive
			}
		});
		return this.send_promise<schema.RequestCreateDirectory, schema.ResponseCreateDirectory>(request);
	}

	move_path(process_id: number, source: string, target: string): Promise<schema.ResponseMovePath['data']> {
		const request = this.prepare_request<schema.RequestMovePath>({
			type: 'move_path', data: {
				process_id: process_id, source: source, target: target
			}
		});
		return this.send_promise<schema.RequestMovePath, schema.ResponseMovePath>(request);
	}

	remove_path(process_id: number, path: string, recursive: boolean): Promise<schema.ResponseRemovePath['data']> {
		const request = this.prepare_request<schema.RequestRemovePath>({
			type: 'remove_path', data: {
				process_id: process_id, path: path, recursive: recursive
			}
		});
		return this.send_promise<schema.RequestRemovePath, schema.ResponseRemovePath>(request);
	}

	open_file(process_id: number, path: string, write: boolean): Promise<schema.ResponseOpenFile['data']> {
		const request = this.prepare_request<schema.RequestOpenFile>({
			type: 'open_file', data: {
				process_id: process_id, path: path, write: write
			}
		});
		return this.send_promise<schema.RequestOpenFile, schema.ResponseOpenFile>(request);
	}

	close_file(process_id: number, fd: number): Promise<schema.ResponseCloseFile['data']> {
		const request = this.prepare_request<schema.RequestCloseFile>({
			type: 'close_file', data: {
				process_id: process_id, fd: fd
			}
		});
		return this.send_promise<schema.RequestCloseFile, schema.ResponseCloseFile>(request);
	}

	read_file_chunk(process_id: number, fd: number, start: number, length: number, callback: (data: FileChunk) => void): Promise<void> {
		return new Promise((resolve, reject) => {
			const request = this.prepare_request<schema.RequestReadFileChunk>({
				type: 'read_file_chunk',
				data: {
					process_id: process_id, fd: fd, start: start, length: length
				}
			});

			this.on<schema.ResponseReadFileChunk>('read_file_chunk', (data) => {
				if (data.buffer != null)
					callback(Object.assign({}, data, { buffer: Buffer.from(data.buffer, 'base64') }));

				if (data.eof) {
					this.off('read_file_chunk', request.request_id);
					resolve();
				}
			}, (error) => {
				this.off('read_file_chunk', request.request_id);
				reject(error);
			}, request.request_id);

			this.send(request);
		});
	}

	write_file_chunk(process_id: number, fd: number, start: number, length: number, buffer: Buffer | null, eof?: boolean): Promise<schema.ResponseWriteFileChunk['data']> {
		const request_data: schema.RequestWriteFileChunk['data'] = { process_id: process_id, fd: fd, start: start, length: length, buffer: null, eof: true };
		if (buffer != null) {
			request_data.buffer = buffer.toString('base64');
			request_data.eof = Boolean(eof);
		}

		const request = this.prepare_request<schema.RequestWriteFileChunk>({
			type: 'write_file_chunk',
			data: request_data
		});
		return this.send_promise<schema.RequestWriteFileChunk, schema.ResponseWriteFileChunk>(request);
	}

	watch_file(process_id: number, fd: number, callback: (data: schema.ResponseWatchFile['data']) => void): Promise<schema.ResponseWatchFile['data']> {
		return new Promise((resolve, reject) => {
			const request = this.prepare_request<schema.RequestWatchFile>({
				type: 'watch_file',
				data: { process_id: process_id, fd: fd }
			});

			let started = false;

			this.on<schema.ResponseWatchFile>('watch_file', (data) => {
				if (data.event_type == 'end')
					this.off('watch_file', request.request_id);
				else if (started)
					callback(data);
				else if (data.event_type == 'start') {
					started = true;
					resolve(data);
				}
				// received data before start event is not expected and will be ignored
			}, (error) => {
				this.off('watch_file', request.request_id);
				reject(error);
			}, request.request_id);

			this.send(request);
		});
	}

	unwatch_file(process_id: number, fd: number): Promise<schema.ResponseUnwatchFile['data']> {
		const request = this.prepare_request<schema.RequestUnwatchFile>({
			type: 'unwatch_file',
			data: { process_id: process_id, fd: fd }
		});
		return this.send_promise<schema.RequestUnwatchFile, schema.ResponseUnwatchFile>(request);
	}
}
