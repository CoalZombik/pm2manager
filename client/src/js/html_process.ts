import $ from 'jquery';
import * as schema from 'pm2manager_schema';
import { ProcessMonit, ProcessMonitRequest } from './process_monit';
import { format_memory, PROC_STATUS_TEXT, timestamp_to_time } from './utils';

export class HtmlProcess {
	static readonly TEMPLATE_PATH = "#templates > .process";

	readonly db_id: number;
	readonly element: JQuery;

	readonly process_name: JQuery;
	readonly version: JQuery;
	readonly commit: JQuery;
	readonly uptime: JQuery;

	readonly status: JQuery;
	readonly cpu: JQuery;
	readonly memory: JQuery;

	readonly buttons: { [key: string]: JQuery };

	readonly info_db_id: JQuery;
	readonly info_pm2_id: JQuery;
	readonly info_pid: JQuery;
	readonly info_user_mode: JQuery;
	readonly info_exec_mode: JQuery;
	readonly info_cwd: JQuery;
	readonly info_exec_path: JQuery;
	readonly info_args: JQuery;
	readonly info_autorestart: JQuery;
	readonly info_restart_count: JQuery;
	readonly info_stdout: JQuery;
	readonly info_stderr: JQuery;
	readonly info_pm2m_log: JQuery;

	process_monit: ProcessMonit;

	last_update: number;
	current_data?: schema.ProcInfo;

	constructor(db_id: number, element: JQuery, monit_request: ProcessMonitRequest) {
		this.db_id = db_id;
		this.element = element;
		element.attr("data-db-id", db_id);

		this.process_name = element.find(".process_name");
		this.version = element.find(".process_version");
		this.commit = element.find(".process_commit");
		this.uptime = element.find(".process_uptime");

		this.status = element.find(".process_status");
		this.cpu = element.find(".process_cpu");
		this.memory = element.find(".process_memory");

		this.buttons = {};
		element.find(".process_buttons").find('button').each((index, element) => {
			const button = $(element);
			const name = button.attr('data-button-type');
			if (name == null)
				return;
			this.buttons[name] = button;
		});

		const process_info = element.find(".process_info");

		this.info_db_id = process_info.find(".process_db_id").val(db_id);
		this.info_pm2_id = process_info.find(".process_pm2_id");
		this.info_pid = process_info.find(".process_pid");
		this.info_user_mode = process_info.find(".process_user_mode");
		this.info_exec_mode = process_info.find(".process_exec_mode");
		this.info_cwd = process_info.find(".process_cwd");
		this.info_exec_path = process_info.find(".process_exec_path");
		this.info_args = process_info.find(".process_args");
		this.info_autorestart = process_info.find(".process_autorestart");
		this.info_restart_count = process_info.find(".process_restart_count");
		this.info_stdout = process_info.find(".process_stdout");
		this.info_stderr = process_info.find(".process_stderr");
		this.info_pm2m_log = process_info.find(".process_pm2m_log");

		this.process_monit = new ProcessMonit(element.find(".process_monit"), db_id, monit_request);

		this.last_update = -1;
		this.current_data = undefined;
	}

	static spawn(process_list: JQuery, db_id: number, monit_request: ProcessMonitRequest): HtmlProcess {
		const process = HtmlProcess.find(process_list, db_id, monit_request);
		if (process)
			return process;

		const element = $(this.TEMPLATE_PATH).clone().appendTo(process_list);
		return new HtmlProcess(db_id, element, monit_request);
	}

	static find(process_list: JQuery, db_id: number, monit_request: ProcessMonitRequest): HtmlProcess | undefined {
		const element = process_list.find(".process[data-db-id=" + db_id + "]");
		if (element.length == 0)
			return undefined;
		return new HtmlProcess(db_id, element, monit_request);
	}

	is_running(): boolean {
		if (!this.current_data)
			return false;

		if (this.current_data.status == schema.ProcStatus.online || this.current_data.status == schema.ProcStatus.launching)
			return true;
		return false;
	}

	on_click(type: string, callback: (db_id: number, html_process: HtmlProcess) => void): boolean {
		const button = this.buttons[type];
		if (button == null)
			return false;

		button.on("click", () => { callback(this.db_id, this); });
		return true;
	}

	update(data: schema.ProcInfo): void {
		if (data.db_id != this.db_id)
			throw new Error("Invalid db_id from data");

		this.last_update = Date.now();
		this.current_data = data;

		if (this.is_running())
			this.element.addClass('running').removeClass('stopped');
		else if (data.status == schema.ProcStatus.not_spawned || data.status == schema.ProcStatus.unknown)
			this.element.removeClass('running').removeClass('stopped');
		else
			this.element.removeClass('running').addClass('stopped');

		this.process_name.text(data.name);
		this.version.text(data.version != null ? data.version : "");
		this.commit.text(data.commit != null ? ("(" + data.commit.sha1.substring(0, 8) + ")") : "");
		this.uptime.text(timestamp_to_time(this.is_running() && data.last_start_time != null ? (this.last_update - data.last_start_time) / 1000 : 0));

		this.status.text(PROC_STATUS_TEXT[data.status]);
		this.cpu.text((100 * (data.cpu != null ? data.cpu : 0)).toFixed(1) + " %");
		this.memory.text(format_memory(data.memory != null ? data.memory : 0));

		this.info_db_id.text(data.db_id);
		this.info_pm2_id.text(data.pm2_id != null ? data.pm2_id : "");
		this.info_pid.text(data.pid != null ? data.pid : "");
		this.info_user_mode.text(data.uid != null && data.gid != null ? data.uid + ":" + data.gid : "");
		this.info_exec_mode.text(data.exec_mode != null ? data.exec_mode : "");
		this.info_cwd.text(data.cwd);
		this.info_exec_path.text(data.exec_path);
		this.info_args.text(data.args.join(", "));
		this.info_autorestart.text(data.autorestart ? "true" : "false");
		this.info_restart_count.text(data.restart_count != null ? data.restart_count : 0);
		this.info_stdout.text(data.logs.stdout ?? "");
		this.info_stderr.text(data.logs.stderr ?? "");
		this.info_pm2m_log.text(data.logs.pm2m);
	}

	delete(): void {
		this.element.remove();
	}
}