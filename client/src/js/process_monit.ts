import $ from 'jquery';
import ApexCharts, { ApexOptions } from 'apexcharts';
import { format_memory, PROC_STATUS_TEXT, show_error, TimedInterval } from './utils';
import * as schema from 'pm2manager_schema';

export interface ChartOptions {
	/** Name of chart displayed as header */
	name: string,
	/** Internal unique id of chart */
	id: string,
	/** Group name if chart will be part of some group with synchronized view */
	group?: string,

	/** Time (is second) between two values of data (of undefined if this function is supposed to be turned off) - if time between two elements will be greater than 2*timestep, chart line will be splitted to segments */
	timestep?: number;

	/** Function that return proper chart value (y axis) from data element */
	accessor: (element: schema.MonitData) => number | null,
	/** Function for format value (y axis) to readable string */
	formatter: (value: number) => string,

	/** Minimum value in x axis */
	x_min?: number,
	/** Maximum value in x axis */
	x_max?: number,
	/** Minimum value in y axis */
	y_min?: number,
	/** Maximum value in y axis */
	y_max?: number
}

export class Chart {
	readonly container: JQuery;
	private options: ChartOptions;

	private chart: ApexCharts;

	/**
	 * 
	 * @param container HTML element used to draw a chart
	 * @param name header of this chart
	 * @param data (optional) starting chart values
	 */
	constructor(container: JQuery, options: ChartOptions, data?: schema.MonitData[]) {
		this.container = container;
		this.options = options;

		this.chart = new ApexCharts(this.container[0], Object.assign({ series: this.convert_to_series(data || []) }, this.generate_chart_options()));
		this.chart.render();
	}

	/** Generate chart options without series attribute */
	private generate_chart_options(): ApexOptions {
		return {
			chart: {
				type: "area",
				id: this.options.id,
				group: this.options.group,
				animations: {
					enabled: false
				}
			},
			stroke: {
				curve: 'smooth'
			},
			xaxis: {
				type: 'datetime',
				min: this.options.x_min,
				max: this.options.x_max,
				labels: {
					datetimeFormatter: {
						year: "yyyy",
						month: "MMM yyyy",
						day: "dd. MM. yyyy",
						hour: "HH:mm",
						minute: "HH:mm:ss"
					}
				}
			},
			yaxis: {
				labels: {
					formatter: this.options.formatter,
					minWidth: 60
				},
				min: this.options.y_min,
				max: this.options.y_max
			},
			dataLabels: {
				enabled: false
			},
			tooltip: {
				enabled: true,
				x: {
					format: "dd. MM. yyyy HH:mm:ss"
				}
			},
			title: {
				text: this.options.name,
				align: 'center'
			}
		};
	}

	private convert_to_series(data: schema.MonitData[]): ApexAxisChartSeries {
		const series_data: [number, number | null][] = [];

		for (let i = 0; i < data.length; i++) {
			const element = data[i];
			if (i > 0 && this.options.timestep != null && element.time - data[i - 1].time > this.options.timestep * 2)
				series_data.push([(element.time - this.options.timestep) * 1000, null]);

			series_data.push([element.time * 1000, this.options.accessor(element)]);
		}

		console.log(series_data);

		return [{ name: this.options.name, data: series_data }];
	}

	private convert_to_annotations(events: schema.MonitEvent[]): ApexAnnotations {
		const annotations: ApexAnnotations = {
			xaxis: []
		};

		events.forEach((element) => {
			annotations.xaxis?.push({
				x: element.time * 1000,
				label: {
					text: element.event_type == schema.ProcStatus.custom ? (element.description ?? "Custom event") : PROC_STATUS_TEXT[element.event_type]
				}
			});
		});

		return annotations;
	}

	update(changes: { data?: schema.MonitData[], events?: schema.MonitEvent[], options?: Partial<ChartOptions> }): Promise<void> {
		Object.assign(this.options, changes.options);
		const chart_options = this.generate_chart_options();
		if (changes.data)
			chart_options.series = this.convert_to_series(changes.data);
		if (changes.events)
			chart_options.annotations = this.convert_to_annotations(changes.events);

		return this.chart.updateOptions(chart_options, changes.data != null, true, false); //fully redraw lines and disable draw of another group charts
	}

	destroy(): void {
		this.chart.destroy();
		this.container.remove();
	}
}

export type ProcessMonitRequest = (process_id: number, start: number, end: number) => Promise<schema.ResponseProcessMonitData>;

export class ProcessMonit {
	static readonly UPDATE_INTERVAL = 60000;
	static readonly UPDATE_DELAY = 5000;

	process_id: number;

	container: JQuery;
	start_date: JQuery;
	start_time: JQuery;
	range: JQuery;

	now_button: JQuery;
	load_button: JQuery;

	charts_container: JQuery;
	charts: { [key: string]: Chart };

	current_custom_data_types?: { [key: string]: string };
	current_data: schema.MonitData[];
	current_events: schema.MonitEvent[];
	current_x_min?: number;
	current_x_max?: number;

	private request: ProcessMonitRequest;
	private request_interval: TimedInterval | null;

	constructor(container: JQuery, process_id: number, request_callback: ProcessMonitRequest) {
		this.process_id = process_id;
		this.container = container;
		this.request = request_callback;

		this.start_date = container.find(".process_monit_date");
		this.start_time = container.find(".process_monit_time");
		this.range = container.find(".process_monit_range");

		this.now_button = container.find(".process_monit_now");
		this.now_button.on('click', () => { this.show_now(); });
		this.load_button = container.find(".process_monit_load");
		this.load_button.on('click', () => { this.clicked_load(); });

		this.charts_container = container.find(".process_monit_charts");

		this.charts = {};
		this.current_data = [];
		this.current_events = [];

		this.request_interval = null;
	}

	private create_chart(key: string, options: ChartOptions): Chart {
		const chart_container = $("<div>", {
			class: "process_monit_chart"
		}).appendTo(this.charts_container);

		const chart = new Chart(chart_container, Object.assign({ group: "charts-" + this.process_id, timestep: ProcessMonit.UPDATE_INTERVAL / 1000 }, options), this.current_data);
		this.charts[key] = chart;
		return chart;
	}

	private create_memory_chart(): Chart {
		return this.create_chart("memory", {
			name: "Memory usage",
			id: "memory",
			accessor: (element) => element.memory,
			formatter: format_memory,
			y_min: 0
		});
	}

	private create_cpu_chart(): Chart {
		return this.create_chart("cpu", {
			name: "CPU usage",
			id: "cpu",
			accessor: (element) => element.cpu,
			formatter: (value) => (value * 100).toFixed(2) + " %",
			y_min: 0
		});
	}

	private create_custom_chart(key: string): Chart {
		return this.create_chart(key, {
			name: "Custom " + key,
			id: key,
			accessor: (element) => element.custom != null ? element.custom[key] : null,
			formatter: (value) => value.toString()
		});
	}

	private clicked_load(): void {
		const start_date_val = this.start_date.val();
		const start_time_val = this.start_time.val() ?? "00:00";
		const range_val = Number(this.range.val());

		if (start_date_val == null) {
			this.show_now();
			return;
		}

		this.stop_autofetch();
		const start = new Date(start_date_val.toString() + " " + start_time_val.toString()).getTime() / 1000;
		const end = start + range_val;

		this.request(this.process_id, start, end).then((data) => {
			this.set_data(data, start * 1000, end * 1000);
		}).catch(show_error);
	}

	private check_charts_exists(): void {
		if (this.charts["memory"] == null)
			this.create_memory_chart();
		if (this.charts["cpu"] == null)
			this.create_cpu_chart();

		if (this.current_custom_data_types != null) {
			Object.keys(this.current_custom_data_types).forEach(key => {
				if (this.charts[key] == null)
					this.create_custom_chart(key);
			});
		}
	}

	private fetch_now(): void {
		const range_val = Number(this.range.val());

		const end = Date.now() / 1000;
		const start = end - range_val;

		this.request(this.process_id, start, end).then((data) => { this.set_data(data, start * 1000, end * 1000); });
	}

	show_now(): void {
		if (this.request_interval && this.request_interval.is_active())
			return;
		this.request_interval = new TimedInterval(() => { this.fetch_now(); }, ProcessMonit.UPDATE_INTERVAL, { modulo: ProcessMonit.UPDATE_INTERVAL, offset: ProcessMonit.UPDATE_DELAY }, true);
	}

	stop_autofetch(): void {
		if (this.request_interval && this.request_interval.is_active()) {
			this.request_interval.clear();
			this.request_interval = null;
		}
	}

	set_data(data: schema.ResponseProcessMonitData, x_min?: number, x_max?: number): void {
		this.current_custom_data_types = data.custom_data_types;
		this.current_data = data.data;
		this.current_events = data.events;
		this.current_x_min = x_min;
		this.current_x_max = x_max;
		this.redraw();
	}

	redraw(): void {
		this.check_charts_exists();
		Object.values(this.charts).forEach((chart) => {
			chart.update({
				options: { x_min: this.current_x_min, x_max: this.current_x_max },
				data: this.current_data,
				events: this.current_events
			});
		});
	}

	clear(): void {
		Object.values(this.charts).forEach((chart) => {
			chart.destroy();
		});
	}
}