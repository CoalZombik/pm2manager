const sass = require('sass');
const path = require('path');
require('dotenv').config();

module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt, {
		pattern: 'grunt-*',
		scope: 'devDependencies',
		requireResolution: true
	});

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			options: {
				implementation: sass,
				sourceMap: true
			},
			dist: {
				files: {
					'build/css/main.css': 'src/css/main.scss'
				}
			}
		},
		browserify: {
			options: {
				browserifyOptions: {
					debug: true
				},
				cacheFile: 'cache/browserify-cache.json',
				configure: b => b.plugin('tsify', { project: __dirname }).transform('loose-envify', { _: "purge" })
				/*configure: b => b.plugin('tsify', { project: __dirname }).plugin('tinyify', {
					env: process.env
				})*/
			},
			dist: {
				files: {
					'build/js/main.js': 'src/js/main.ts'
				}
			}
		},
		copy: {
			dist: {
				expand: true,
				cwd: 'src',
				src: ['fonts/**/*', '**/*.html'],
				dest: 'build'
			}
		},
		watch: {
			scripts: {
				files: 'src/js/**/*.ts',
				tasks: ['browserify'],
				options: {
					livereload: true,
					spawn: false
				}
			},
			sass: {
				files: 'src/css/**/*.scss',
				tasks: ['sass'],
				options: {
					livereload: true
				}
			},
			html: {
				files: [
					'src/**/*.html',
					'src/fonts/**/*'
				],
				tasks: ['copy'],
				options: {
					livereload: true
				}
			},
			configFiles: {
				files: ['gruntfile.js', '.env', 'package.json'],
				options: {
					reload: true
				}
			}
		}
	});

	grunt.registerTask('default', ['browserify', 'sass', 'copy']);
};