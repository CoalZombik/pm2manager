# pm2manager 0.3.2

Websocket API and web frontend for managing pm2 services.

## Build

1. Prepare yarn environment.
   
   1. Install [yarn berry](https://github.com/yarnpkg/berry).
   
   2. Import workspaces plugin `yarn plugin import workspace-tools`.
   
   3. (Optional) Setup [SDKs](https://yarnpkg.com/getting-started/editor-sdks) for your IDE.

2. Download required node packages via `yarn install`.

3. Build source codes via `yarn build` in main directory.

4. Create configuration files
   
   1. For server-side: Run setup wizard from `server` directory by calling `yarn setup`
   
   2. For client-side: Copy `client/.default.env` to `client/.env`, modify variables and rebuild `client` workspace.

## Run

After finishing a build, websocket server can be started by running `yarn start` in `server` directory. Must be run by privileged user to offer user/group switching.

Client-side is built in `client/build`. Copy these files to your web server or open `client/build/index.html` in web browser.
