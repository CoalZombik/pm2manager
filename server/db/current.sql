SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE TABLE `info` (
  `DbVersion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `info` (`DbVersion`) VALUES (1);

CREATE TABLE `monit_data` (
  `MonitDataID` int(11) NOT NULL AUTO_INCREMENT,
  `ProcessID` int(11) NOT NULL,
  `Time` datetime NOT NULL,
  `CPU` float NOT NULL,
  `Memory` bigint(20) NOT NULL,
  `CustomData` text DEFAULT NULL,
  PRIMARY KEY (`MonitDataID`),
  KEY `ProcessID` (`ProcessID`),
  KEY `Timestamp` (`Time`),
  CONSTRAINT `monit_data_ibfk_1` FOREIGN KEY (`ProcessID`) REFERENCES `processes` (`ProcessID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `monit_events` (
  `MonitEventID` int(11) NOT NULL AUTO_INCREMENT,
  `ProcessID` int(11) NOT NULL,
  `Time` datetime NOT NULL,
  `EventType` smallint(6) NOT NULL,
  `Description` text DEFAULT NULL,
  PRIMARY KEY (`MonitEventID`),
  KEY `ProcessID` (`ProcessID`),
  KEY `Time` (`Time`),
  CONSTRAINT `monit_events_ibfk_1` FOREIGN KEY (`ProcessID`) REFERENCES `processes` (`ProcessID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `processes` (
  `ProcessID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `ExecPath` varchar(4096) NOT NULL,
  `Directory` varchar(4096) NOT NULL,
  `Config` text NOT NULL,
  `DeployMethod` smallint(6) NOT NULL DEFAULT 0,
  `DeploySecret` char(16) DEFAULT NULL,
  `DeployBranch` varchar(255) DEFAULT NULL,
  `DeployUseSSH` tinyint(1) NOT NULL DEFAULT 0,
  `DeployPrivateKey` blob DEFAULT NULL,
  `DeployBuildStage` varchar(255) DEFAULT NULL,
  `LogCustomTypes` text DEFAULT NULL,
  PRIMARY KEY (`ProcessID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `tokens` (
  `TokenID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `ValidUntil` datetime NOT NULL,
  `Token` char(16) NOT NULL,
  `IP` varchar(255) NOT NULL,
  PRIMARY KEY (`TokenID`),
  KEY `UserID` (`UserID`),
  KEY `Token` (`Token`),
  CONSTRAINT `tokens_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `LastLogin` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Admin` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `users_process_perm` (
  `PermissionID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `ProcessID` int(11) NOT NULL,
  `View` tinyint(1) NOT NULL DEFAULT 0,
  `Env` tinyint(1) NOT NULL DEFAULT 0,
  `FileRead` tinyint(1) NOT NULL DEFAULT 0,
  `FileWrite` tinyint(1) NOT NULL DEFAULT 0,
  `Manage` tinyint(1) NOT NULL DEFAULT 0,
  `Log` tinyint(1) NOT NULL DEFAULT 0,
  `Deploy` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`PermissionID`),
  KEY `UserID` (`UserID`),
  KEY `ProcessID` (`ProcessID`),
  CONSTRAINT `users_process_perm_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`) ON DELETE CASCADE,
  CONSTRAINT `users_process_perm_ibfk_2` FOREIGN KEY (`ProcessID`) REFERENCES `processes` (`ProcessID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
