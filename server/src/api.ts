import { User } from './user';
import { UserProcess } from './user_process';
import { Process } from './process';
import { ApiError } from './api_error';
import * as schema from 'pm2manager_schema';
import { Database } from './database';
import Ajv, { ValidateFunction } from 'ajv';
import fs from 'fs';
import { ConfigLoader } from './config';

type ApiFunction = (request: schema.Request, database: Database, process_map: Map<number, Process>) => Promise<schema.Response | null> | (schema.Response | null);
type ApiFunctionName = `api_${schema.RequestType}`;

type ApiFunctionSet = {
	[key in ApiFunctionName]: ApiFunction
};

export class ConnectedUser implements ApiFunctionSet {
	login_id: number;
	ip_addr: string;
	send_method: (data: schema.ApiSchema) => void;

	user?: User;
	process_map: Map<number, UserProcess>;

	constructor(login_id: number, ip_addr: string, send_method: (data: schema.ApiSchema) => void) {
		this.login_id = login_id;
		this.user = undefined;
		this.process_map = new Map();

		this.ip_addr = ip_addr;
		this.send_method = send_method;
	}

	destroy(): void {
		this.process_map.forEach(process => {
			process.destroy();
		});
	}

	/**
	 * Check if user is authentified
	 */
	private check_user(): void {
		if (!this.user)
			throw new ApiError(401, "User is not authenticated");
	}

	/**
	 * @param {number} process_id 
	 * @returns {UserProcess}
	 */
	private find_process(process_id: number): UserProcess {
		const process = this.process_map.get(process_id);
		if (!process)
			throw new ApiError(404, "Process is not exists");
		return process;
	}

	/** Execute function with specific api type */
	async exec(request: schema.Request, params: Parameters<ApiFunction>): Promise<schema.Response | null> {
		const callback = this[("api_" + request.type) as ApiFunctionName] as ApiFunction;

		const retval = callback.call(this, ...params);
		if (retval instanceof Promise)
			return await retval;
		return retval;
	}

	async api_auth(request: schema.Request, database: Database, process_map: Map<number, Process>): Promise<schema.ResponseAuth> {
		const data = request.data as schema.RequestAuth['data'];

		const connection = await database.get_connection();
		try {
			if ('token' in data)
				this.user = await User.load_by_token(connection, data.token, this.ip_addr);
			else
				this.user = await User.load_by_login(connection, data.username, data.password, this.ip_addr);

			this.process_map = await UserProcess.load_all_for_user(connection, process_map, this.user);

			return {
				status: 200, type: 'auth', request_id: request.request_id,
				data: { username: this.user.username, token: this.user.used_token, valid_until: this.user.token_valid_time }
			};
		} finally {
			connection.close();
		}
	}

	api_get_process_list(request: schema.Request): schema.ResponseGetProcessList {
		this.check_user();

		const info_list: schema.ProcInfo[] = [];
		this.process_map.forEach((process) => {
			if (process.view)
				info_list.push(process.get_app_info());
		});

		return {
			status: 200, type: 'get_process_list', request_id: request.request_id,
			data: { info: info_list }
		};
	}

	api_get_process_info(request: schema.Request): schema.ResponseGetProcessInfo {
		const data = request.data as schema.RequestGetProcessInfo['data'];
		this.check_user();

		return {
			status: 200, type: 'get_process_info', request_id: request.request_id,
			data: { info: this.find_process(data.process_id).get_app_info() }
		};
	}

	async api_get_process_monit(request: schema.Request, database: Database): Promise<schema.ResponseGetProcessMonit> {
		const data = request.data as schema.RequestGetProcessMonit['data'];
		this.check_user();

		const connection = await database.get_connection();
		const process = this.find_process(data.process_id);
		const monit_info = await process.get_monit_info(connection, data.start, data.end);
		connection.close();

		return {
			status: 200, type: 'get_process_monit', request_id: request.request_id,
			data: {
				process_id: data.process_id,
				custom_data_types: process.process.log_custom_types,
				data: monit_info.data,
				events: monit_info.event
			}
		};
	}

	async api_open_log(request: schema.Request): Promise<schema.ResponseOpenLog> {
		const data = request.data as schema.RequestOpenLog['data'];
		this.check_user();

		const process = this.find_process(data.process_id);
		const info = await process.open_log(data.log_type);

		return {
			status: 200, type: 'open_log', request_id: request.request_id,
			data: { path: info.path, process_id: data.process_id, fd: info.index, size: info.size }
		};
	}

	async api_start_process(request: schema.Request): Promise<schema.ResponseStartProcess> {
		const data = request.data as schema.RequestStartProcess['data'];
		this.check_user();

		await this.find_process(data.process_id).start();
		return { status: 200, type: 'start_process', request_id: request.request_id, data: data };
	}

	async api_stop_process(request: schema.Request): Promise<schema.ResponseStopProcess> {
		const data = request.data as schema.RequestStopProcess['data'];
		this.check_user();

		await this.find_process(data.process_id).stop();
		return { status: 200, type: 'stop_process', request_id: request.request_id, data: data };
	}

	api_get_env(request: schema.Request): schema.ResponseGetEnv {
		const data = request.data as schema.RequestGetEnv['data'];
		this.check_user();

		const env = this.find_process(data.process_id).get_env();
		return {
			status: 200, type: 'get_env', request_id: request.request_id,
			data: { process_id: data.process_id, env: env }
		};
	}

	async api_set_env(request: schema.Request, database: Database): Promise<schema.ResponseSetEnv> {
		const data = request.data as schema.RequestSetEnv['data'];
		this.check_user();

		const connection = await database.get_connection();
		await this.find_process(data.process_id).set_env(connection, data.env);
		connection.close();

		return {
			status: 200, type: 'set_env', request_id: request.request_id,
			data: { process_id: data.process_id }
		};
	}

	api_get_deploy(request: schema.Request): schema.ResponseGetDeploy {
		const data = request.data as schema.RequestGetDeploy['data'];
		this.check_user();

		return {
			status: 200, type: 'get_deploy', request_id: request.request_id,
			data: this.find_process(data.process_id).get_deploy_info()
		};
	}

	async api_set_deploy(request: schema.Request, database: Database): Promise<schema.ResponseSetDeploy> {
		const data = request.data as schema.RequestSetDeploy['data'];
		this.check_user();

		const connection = await database.get_connection();
		await this.find_process(data.process_id).set_deploy_info(connection, data);
		connection.close();

		return {
			status: 200, type: 'set_deploy', request_id: request.request_id,
			data: { process_id: data.process_id }
		};
	}

	async api_reset_deploy_secret(request: schema.Request, database: Database): Promise<schema.ResponseResetDeploySecret> {
		const data = request.data as schema.RequestResetDeploySecret['data'];
		this.check_user();

		const connection = await database.get_connection();
		const new_secret = await this.find_process(data.process_id).generate_deploy_secret(connection);
		connection.close();

		return {
			status: 200, type: 'reset_deploy_secret', request_id: request.request_id,
			data: { process_id: data.process_id, secret: new_secret }
		};
	}

	async api_reset_deploy_key(request: schema.Request, database: Database): Promise<schema.ResponseResetDeployKey> {
		const data = request.data as schema.RequestResetDeployKey['data'];
		this.check_user();

		const connection = await database.get_connection();
		const new_key = await this.find_process(data.process_id).generate_deploy_key(connection);
		connection.close();

		return {
			status: 200, type: 'reset_deploy_key', request_id: request.request_id,
			data: { process_id: data.process_id, ssh_key: new_key }
		};
	}

	async api_build(request: schema.Request): Promise<schema.ResponseBuild> {
		const data = request.data as schema.RequestBuild['data'];
		this.check_user();

		await this.find_process(data.process_id).build();
		return {
			status: 200, type: 'build', request_id: request.request_id,
			data: { process_id: data.process_id }
		};
	}

	api_list_directory(request: schema.Request): schema.ResponseListDirectory {
		const data = request.data as schema.RequestListDirectory['data'];
		this.check_user();

		const content = this.find_process(data.process_id).list_directory(data.path);

		return {
			status: 200, type: 'list_directory', request_id: request.request_id,
			data: { process_id: data.process_id, path: data.path, content: content }
		};
	}

	api_create_directory(request: schema.Request): schema.ResponseCreateDirectory {
		const data = request.data as schema.RequestCreateDirectory['data'];
		this.check_user();

		this.find_process(data.process_id).create_directory(data.path, data.recursive);

		return { status: 200, type: 'create_directory', request_id: request.request_id, data: data };
	}

	api_move_path(request: schema.Request): schema.ResponseMovePath {
		const data = request.data as schema.RequestMovePath['data'];
		this.check_user();

		this.find_process(data.process_id).move_path(data.source, data.target);

		return { status: 200, type: 'move_path', request_id: request.request_id, data: data };
	}

	api_remove_path(request: schema.Request): schema.ResponseRemovePath {
		const data = request.data as schema.RequestRemovePath['data'];
		this.check_user();

		this.find_process(data.process_id).remove_path(data.path, data.recursive);

		return { status: 200, type: 'remove_path', request_id: request.request_id, data: data };
	}

	async api_open_file(request: schema.Request): Promise<schema.ResponseOpenFile> {
		const data = request.data as schema.RequestOpenFile['data'];
		this.check_user();

		const info = await this.find_process(data.process_id).open_file(data.path, data.write);

		return {
			status: 200, type: 'open_file', request_id: request.request_id,
			data: { path: data.path, process_id: data.process_id, fd: info.index, size: info.size }
		};
	}

	async api_close_file(request: schema.Request): Promise<schema.ResponseCloseFile> {
		const data = request.data as schema.RequestCloseFile['data'];
		this.check_user();

		await this.find_process(data.process_id).close_file(data.fd);

		return { status: 200, type: 'close_file', request_id: request.request_id, data: data };
	}

	async api_read_file_chunk(request: schema.Request): Promise<null> {
		const data = request.data as schema.RequestReadFileChunk['data'];
		this.check_user();

		await this.find_process(data.process_id).read_file_chunk(data.fd, data.start, data.length, (chunk) => {
			const response: schema.ResponseReadFileChunk = {
				status: 200, type: 'read_file_chunk', request_id: request.request_id,
				data: {
					process_id: data.process_id,
					fd: data.fd,
					start: chunk.start,
					length: chunk.length,
					buffer: chunk.buffer != null ? chunk.buffer.toString('base64') : null,
					eof: chunk.eof
				}
			};
			this.send_method(response);
		});

		return null;
	}

	async api_write_file_chunk(request: schema.Request): Promise<schema.ResponseWriteFileChunk> {
		const data = request.data as schema.RequestWriteFileChunk['data'];
		this.check_user();

		const process = this.find_process(data.process_id);

		if (data.buffer == null && !data.eof)
			throw new ApiError(400, "Buffer is null, but eof is false");

		let written = 0;

		if (data.buffer != null)
			written = await process.write_file_chunk(data.fd, Buffer.from(data.buffer, 'base64'), data.start, data.length);

		if (data.eof)
			await process.file_truncate(data.fd, data.start + data.length);

		return {
			status: 200, type: 'write_file_chunk', request_id: request.request_id,
			data: { process_id: data.process_id, fd: data.fd, start: data.start, length: written }
		};
	}

	async api_watch_file(request: schema.Request): Promise<schema.ResponseWatchFile> {
		const data = request.data as schema.RequestWatchFile['data'];
		this.check_user();

		const process = this.find_process(data.process_id);

		const size = await process.watch_file(data.fd, (event_type, size) => {
			const response: schema.ResponseWatchFile = {
				status: 200, type: 'watch_file', request_id: request.request_id,
				data: { process_id: data.process_id, fd: data.fd, event_type: event_type, size: size }
			};
			this.send_method(response);
		}, () => {
			const response: schema.ResponseWatchFile = {
				status: 200, type: 'watch_file', request_id: request.request_id,
				data: { process_id: data.process_id, fd: data.fd, event_type: 'end', size: null }
			};
			this.send_method(response);
		});

		return {
			status: 200, type: 'watch_file', request_id: request.request_id,
			data: { process_id: data.process_id, fd: data.fd, event_type: 'start', size: size }
		};
	}

	api_unwatch_file(request: schema.Request): schema.ResponseUnwatchFile {
		const data = request.data as schema.RequestUnwatchFile['data'];
		this.check_user();

		const process = this.find_process(data.process_id);
		process.unwatch_file(data.fd);

		return { status: 200, type: 'unwatch_file', request_id: request.request_id, data: data };
	}

	api_error(): never {
		throw new ApiError(400, "Invalid request type");
	}
}

export class Api {
	database: Database;
	connected_users: Map<number, ConnectedUser>;
	process_map: Map<number, Process>;

	validate_api: ValidateFunction<schema.ApiSchema>;

	next_login_id: number;

	token_cleanup?: NodeJS.Timer;

	constructor(database: Database, process_map: Map<number, Process>) {
		this.database = database;
		this.connected_users = new Map();
		this.process_map = process_map;

		this.next_login_id = 0;

		const ajv = new Ajv({ logger: console });
		ajv.addKeyword("tsEnumNames");
		const schema = JSON.parse(fs.readFileSync("../schema/api.schema.json", { encoding: 'utf-8' })); //TODO: Configurable schema location
		this.validate_api = ajv.compile<schema.ApiSchema>(schema);

		const token_clean_interval = ConfigLoader.config.api.token_clean_interval;
		if (token_clean_interval > 0) {
			this.database.clear_expired_tokens().catch(console.error);
			this.token_cleanup = setInterval(() => {
				this.database.clear_expired_tokens().catch(console.error);
			}, ConfigLoader.config.api.token_clean_interval * 1000);
		}
	}

	connect_user(ip_addr: string, send_method: (data: schema.ApiSchema) => void): number {
		const connected_user = new ConnectedUser(this.next_login_id++, ip_addr, send_method);
		this.connected_users.set(connected_user.login_id, connected_user);
		return connected_user.login_id;
	}

	disconnect_user(login_id: number): void {
		const connected_user = this.connected_users.get(login_id);

		if (!connected_user)
			throw new Error("Connected user does not exists");

		connected_user.destroy();
		this.connected_users.delete(login_id);
	}

	async process_command(login_id: number, request_json: string): Promise<void> {
		const connected_user = this.connected_users.get(login_id);

		if (!connected_user)
			throw new Error("Connected user does not exists");

		let request: schema.Request | null = null;
		let response: schema.Response | null = null;

		try {
			request = JSON.parse(request_json);
			console.log(request);

			if (!this.validate_api(request)) {
				console.dir(this.validate_api.errors, { depth: null });
				throw new ApiError(400, "Invalid request JSON schema");
			}

			const params: Parameters<ApiFunction> = [request, this.database, this.process_map];
			response = await connected_user.exec(request, params);
		} catch (err) {
			let error: ApiError;

			if (!(err instanceof ApiError)) {
				if (request == null)
					error = new ApiError(400, "Invalid received message", err);
				else
					error = new ApiError(500, "Server-side error", err);
			}
			else
				error = err;

			error.log_error();
			response = error.to_response(request?.type, request?.request_id);
		} finally {
			console.dir(response, { depth: null });
			if (response != null) {
				if (!this.validate_api(response)) {
					const error = new ApiError(500, "Invalid response JSON schema");
					error.log_error();
					response = error.to_response(request?.type, request?.request_id);
				}

				connected_user.send_method(response);
			}
		}
	}

	async update_processes(rotate: boolean): Promise<void> {
		let promises: Promise<void>[] = [];
		this.process_map.forEach(process => {
			promises.push(process.update().catch(reason => { new ApiError(500, "Server-side error", reason).log_error(); })); //TODO: Better error handling
		});

		await Promise.all(promises);

		if (rotate) {
			const connection = await this.database.get_connection();
			connection.transaction_begin();

			promises = [];
			this.process_map.forEach(process => {
				promises.push(process.rotate_monit_log(connection));
			});

			await Promise.all(promises);

			connection.transaction_commit();
			connection.close();
		}
	}
}
