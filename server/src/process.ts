import { Deploy } from './deploy';
import { PM2, ProcessDescription, StartOptions } from './pm2_promise';
import { execSync, spawn } from 'child_process';
import { ApiError } from './api_error';
import { Database, DatabaseConnection, DatabaseMonitData, DatabaseMonitEvent, DatabaseProcess } from './database';
import * as schema from 'pm2manager_schema';
import path from 'path';
import fs from 'fs';
import util from 'util';
import { ConfigLoader } from './config';
import { Console } from 'console';
import { UnixUser } from './utils';
import readline from 'readline';
import semver from 'semver';

type ProcInfoInternal = Omit<schema.ProcInfo, 'db_id' | 'commit'>;

type Pm2Description = ProcessDescription & { pm2_env?: { [key: string]: any } };

const pm2_proc_status_table = {
	online: schema.ProcStatus.online,
	stopping: schema.ProcStatus.stopping,
	stopped: schema.ProcStatus.stopped,
	launching: schema.ProcStatus.launching,
	errored: schema.ProcStatus.errored,
	'one-launch-status': schema.ProcStatus.one_launch_status
};

interface ResolvedRealPath {
	path: string;
	exists: boolean;
	access: boolean;
}

interface YarnJson {
	type: 'info' | 'warning' | 'error',
	name: number | null,
	displayName: string,
	indent: string,
	data: string
}

type ProcessConfig = StartOptions & {
	uid: number,
	gid: number,
	autorestart?: boolean;
};

const access_promise = util.promisify(fs.access);
const close_promise = util.promisify(fs.close);

export interface OpenFileData {
	fd: number,
	path: string,
	watcher?: fs.FSWatcher,
	watcher_close_callback?: () => void;
}

export class MonitData implements schema.MonitData {
	id?: number;
	process_id: number;
	time: number;
	cpu: number;
	memory: number;
	custom?: Record<string, number>;

	constructor(process_id: number, time: number, cpu: number, memory: number, custom_data?: Record<string, number>, id?: number) {
		this.id = id;
		this.process_id = process_id;
		this.time = time;
		this.cpu = cpu;
		this.memory = memory;
		this.custom = custom_data != null && Object.keys(custom_data).length > 0 ? custom_data : undefined;
	}

	static parse_database_row(data: DatabaseMonitData): MonitData {
		return new MonitData(data.ProcessID, data.Time, data.CPU, data.Memory, data.CustomData != null ? JSON.parse(data.CustomData) : null, data.MonitDataID);
	}

	static parse_database_row_array(data: DatabaseMonitData[]): MonitData[] {
		const output: MonitData[] = [];
		data.forEach((row) => {
			output.push(MonitData.parse_database_row(row));
		});

		return output;
	}

	/**
	 * Load monit data between time range from db
	 * 
	 * @param start_time timestamp in second format
	 * @param end_time timestamp in second format
	 */
	static async load_time_range(connection: DatabaseConnection, process_id: number, start_time: number, end_time: number): Promise<MonitData[]> {
		return this.parse_database_row_array(await connection.monit_data_load_range(process_id, start_time, end_time));
	}

	static async load_specific(connection: DatabaseConnection, id: number): Promise<MonitData | undefined> {
		const data = await connection.monit_data_load_specific(id);
		return data != null ? this.parse_database_row(data) : undefined;
	}

	async save(connection: DatabaseConnection): Promise<void> {
		const base_data: Omit<DatabaseMonitData, "MonitDataID"> = {
			ProcessID: this.process_id,
			Time: this.time,
			CPU: this.cpu,
			Memory: this.memory,
			CustomData: this.custom != null ? JSON.stringify(this.custom) : null
		};

		if (this.id != null)
			await connection.monit_data_update(Object.assign({ MonitDataID: this.id }, base_data));
		else
			this.id = await connection.monit_data_insert(base_data);
	}
}

export class MonitEvent implements schema.MonitEvent {
	id?: number;
	process_id: number;
	time: number;
	event_type: schema.ProcStatus;
	description?: string;

	constructor(process_id: number, time: number, status: schema.ProcStatus, description?: string, id?: number) {
		this.id = id;
		this.process_id = process_id;
		this.time = time;
		this.event_type = status;
		this.description = description;
	}

	static parse_database_row(data: DatabaseMonitEvent): MonitEvent {
		return new MonitEvent(data.ProcessID, data.Time, data.EventType, data.Description != null ? data.Description : undefined, data.MonitEventID);
	}

	static parse_database_row_array(data: DatabaseMonitEvent[]): MonitEvent[] {
		const output: MonitEvent[] = [];
		data.forEach((row) => {
			output.push(MonitEvent.parse_database_row(row));
		});

		return output;
	}

	/**
	 * Load monit events between time range from db
	 * 
	 * @param start_time timestamp in second format
	 * @param end_time timestamp in second format
	 */
	static async load_time_range(connection: DatabaseConnection, process_id: number, start_time: number, end_time: number): Promise<MonitEvent[]> {
		return MonitEvent.parse_database_row_array(await connection.monit_event_load_range(process_id, start_time, end_time));
	}

	static async load_specific(connection: DatabaseConnection, id: number): Promise<MonitEvent | undefined> {
		const data = await connection.monit_event_load_specific(id);
		return data != null ? this.parse_database_row(data) : undefined;
	}

	static async load_last(connection: DatabaseConnection, process_id: number): Promise<MonitEvent | undefined> {
		const data = await connection.monit_event_load_last(process_id);
		return data != null ? this.parse_database_row(data) : undefined;
	}

	async save(connection: DatabaseConnection): Promise<void> {
		const base_data: Omit<DatabaseMonitEvent, "MonitEventID"> = {
			ProcessID: this.process_id,
			Time: this.time,
			EventType: this.event_type,
			Description: this.description ?? null
		};

		if (this.id != null)
			await connection.monit_event_update(Object.assign({ MonitEventID: this.id }, base_data));
		else
			this.id = await connection.monit_event_insert(base_data);
	}
}

export class Process {
	static yarn_exec: string;
	static yarn_env: Record<string, string>;

	id: number;
	name: string;
	exec: string;
	dir: string;
	/** args is string[] only */
	config: ProcessConfig;
	deploy: Deploy;
	log_custom_types?: { [key: string]: string; };
	info: ProcInfoInternal;

	last_status: schema.ProcStatus;

	cpu_sum: number;
	memory_sum: number;
	monit_sum_count: number;

	pm2m_log: Console;

	database: Database;

	constructor(row_db: DatabaseProcess, database: Database) {
		this.id = row_db.ProcessID;
		this.name = row_db.Name;
		this.exec = row_db.ExecPath;
		this.dir = row_db.Directory;
		this.config = Object.assign({
			uid: ConfigLoader.config.main.uid,
			gid: ConfigLoader.config.main.gid,
			env: {}
		}, JSON.parse(row_db.Config));
		this.deploy = new Deploy(this, row_db);
		this.log_custom_types = row_db.LogCustomTypes != null ? JSON.parse(row_db.LogCustomTypes) : undefined;

		this.cpu_sum = 0;
		this.memory_sum = 0;
		this.monit_sum_count = 0;

		this.last_status = schema.ProcStatus.not_spawned;
		this.info = this.generate_unspawned_info();

		const fd = fs.openSync(this.info.logs.pm2m, 'a+');
		const stream = fs.createWriteStream('', {
			fd: fd,
			autoClose: true
		});
		this.pm2m_log = new Console({
			stdout: stream,
			colorMode: false
		});

		this.database = database;
	}

	/**
	 * Initialize yarn variables and check version
	 */
	static init_yarn(): void {
		const cwd = process.cwd();

		let yarn_dir = ConfigLoader.config.yarn.directory;
		if (!path.isAbsolute(yarn_dir))
			yarn_dir = path.join(cwd, yarn_dir);

		Process.yarn_exec = path.join(yarn_dir, "releases/yarn-berry.cjs");

		Process.yarn_env = {
			...process.env,
			YARN_ENABLE_GLOBAL_CACHE: ConfigLoader.config.yarn.enable_global_cache ? "1" : "0",
			YARN_GLOBAL_FOLDER: yarn_dir
		};

		const yarn_semver = execSync(Process.yarn_exec + " --version", { encoding: 'utf8' });
		if (!semver.satisfies(yarn_semver, '>=2.0.0'))
			throw new Error("Old yarn version " + yarn_semver + " (required >=2.0.0)");
		console.log("Using yarn " + yarn_semver + "(" + Process.yarn_exec + ")");
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	log_header(message: any, ...params: any[]): void {
		this.pm2m_log.log("> [%s] " + message, new Date().toISOString(), ...params);
	}

	async load_last_status(connection: DatabaseConnection, restore: boolean): Promise<void> {
		const last_status = await MonitEvent.load_last(connection, this.id);
		if (last_status == null)
			return;

		this.last_status = last_status.event_type;
		await this.update(connection);

		this.log_header("Restoring process %d", this.id);
		this.pm2m_log.log("Last status in database %s, current status %s", schema.ProcStatus[last_status.event_type], schema.ProcStatus[this.info.status]);

		if (!restore || (last_status.event_type != schema.ProcStatus.online && last_status.event_type != schema.ProcStatus.launching && last_status.event_type != schema.ProcStatus.updating))
			return;

		try {
			if (this.is_stopped())
				await this.start();
		} catch (e: unknown) {
			this.pm2m_log.error(e);
		}
	}

	/**
	 * Convert pm2 status string to enum.
	 * @returns ProcStatus.unknown if pm_status is not valid
	 */
	private static pm2_to_proc_status(pm_status: string): schema.ProcStatus {
		if (pm_status in pm2_proc_status_table)
			return pm2_proc_status_table[pm_status as keyof typeof pm2_proc_status_table];
		return schema.ProcStatus.unknown;
	}

	/**
	 * Return true if process is stopped or errored
	 */
	is_normal_stopped(): boolean {
		return this.info.status == schema.ProcStatus.not_spawned || this.info.status == schema.ProcStatus.stopped;
	}

	/**
	 * Return true if process is stopped in normal mode (non errored)
	 */
	is_stopped(): boolean {
		return this.is_normal_stopped() || this.info.status == schema.ProcStatus.errored;
	}

	/**
	 * Set process to 'non-spawned' status
	 */
	unspawn(): void {
		if (this.last_status != schema.ProcStatus.not_spawned)
			this.info = this.generate_unspawned_info();
	}

	generate_unspawned_info(): ProcInfoInternal {
		let pm2m_log = path.join(ConfigLoader.config.main.process_pm2m_log_directory, this.id + ".log");
		if (!path.isAbsolute(pm2m_log))
			pm2m_log = path.join(process.cwd(), pm2m_log);

		return {
			name: this.name,
			status: schema.ProcStatus.not_spawned,
			cwd: this.dir,
			exec_path: this.exec,
			args: this.config.args != null ? this.config.args as string[] : [],
			autorestart: this.config.autorestart ?? false,
			logs: {
				pm2m: pm2m_log
			}
		};
	}

	/**
	 * Update monit average data and current monit
	 */
	update_monit(monit: { memory: number, cpu: number }): void {
		this.info.cpu = monit.cpu / 100;
		this.info.memory = monit.memory;

		this.cpu_sum += monit.cpu / 100;
		this.memory_sum += monit.memory;

		this.monit_sum_count += 1;
	}

	/**
	 * Add new monit event if status was changed
	 */
	async update_status_monit_event(connection?: DatabaseConnection): Promise<void> {
		if (this.last_status != this.info.status) {
			try {
				await this.database.run_with_connection((connection) => {
					return new MonitEvent(this.id, Date.now() / 1000, this.info.status).save(connection);
				}, connection);
			} catch (e) {
				new ApiError(500, "Cannot write status event to database", e).log_error();
			}
		}
		this.last_status = this.info.status;
	}

	/**
	 * Score represent how much pm2 description is equal with this process (higher is better).
	 * 
	 * If score is below zero, pm2 process with this description will be removed after process start.
	 */
	compute_pair_score(description: Pm2Description): number {
		let score = 100; //minor differences is accepted
		if (description.pm2_env?.pm_exec_path != null && description.pm2_env.pm_exec_path != this.exec)
			score -= 5000;
		if (description.pm2_env?.pm_cwd != null && description.pm2_env.pm_cwd != this.dir)
			score -= 3000;
		if (this.info.pm2_id != null && this.info.pm2_id != description.pm_id)
			score -= 1000;
		if (this.name != description.name)
			score -= 500;

		if (description.pm2_env?.node_args != null) {
			const description_args = Array.isArray(description.pm2_env?.node_args) ? description.pm2_env?.node_args.join(" ") : description.pm2_env?.node_args;
			const config_args = Array.isArray(this.config.args) ? this.config.args.join(" ") : (this.config.args ?? "");

			if (description_args != config_args)
				score -= 10;
		}

		return score;
	}

	/**
	 * Returns description that represent this process with the best score from `compute_pair_score`.
	 */
	pick_best_description(descriptions: Pm2Description[]): { description: Pm2Description, score: number } | undefined {
		if (descriptions.length == 0)
			return undefined;

		let best_description = descriptions[0];
		let best_score = this.compute_pair_score(best_description);

		for (let i = 1; i < descriptions.length; i++) {
			const current_score = this.compute_pair_score(descriptions[i]);
			if (current_score > best_score) {
				best_description = descriptions[i];
				best_score = current_score;
			}
		}

		return {
			description: best_description,
			score: best_score
		};
	}

	/**
	 * Remove all processes from with pm2 that is not valid
	 */
	async clean_bad_pm2_processes(): Promise<void> {
		const search_name = this.info.pm2_id != null ? this.info.pm2_id : this.name;
		const descriptions = await PM2.describe(search_name);

		descriptions.forEach(description => {
			if (description.pm_id == null) // process without ID can be safely removed
				return;

			const score = this.compute_pair_score(description);
			if (score < 0) {
				this.log_header("Removing pm2 process %s (id %d) with score %d", description.name, description.pm_id, score);
				this.pm2m_log.log(description);
				PM2.delete(description.pm_id);
			}
		});
	}

	/**
	 * Update process info from pm2 data
	 */
	async update(connection?: DatabaseConnection): Promise<void> {
		const search_name = this.info.pm2_id != null ? this.info.pm2_id : this.name;
		const descriptions = await PM2.describe(search_name);

		const description = this.pick_best_description(descriptions)?.description;
		if (description == null) //not spawned
			this.unspawn();
		else {
			this.info.pm2_id = description.pm_id;
			if (description.pm2_env) {
				this.info.version = description.pm2_env.version;
				if (this.info.status != schema.ProcStatus.updating || description.pm2_env.status != 'stopped')
					this.info.status = description.pm2_env.status != null ? Process.pm2_to_proc_status(description.pm2_env.status) : schema.ProcStatus.unknown;
				this.info.last_start_time = description.pm2_env.pm_uptime;

				this.info.pid = description.pid;
				this.info.exec_mode = description.pm2_env.exec_mode;
				this.info.uid = description.pm2_env.uid;
				this.info.gid = description.pm2_env.gid;
				if (description.pm2_env.pm_exec_path != null)
					this.info.exec_path = description.pm2_env.pm_exec_path;
				this.info.args = description.pm2_env.node_args;
				this.info.autorestart = description.pm2_env.autorestart;
				this.info.restart_count = description.pm2_env.restart_time;
				this.info.logs.stdout = description.pm2_env.pm_out_log_path;
				this.info.logs.stderr = description.pm2_env.pm_err_log_path;

				if (description.monit && description.monit.cpu != undefined && description.monit.memory != undefined)
					this.update_monit(description.monit as { cpu: number, memory: number });
				await this.deploy.update_commit_info();
			}
		}

		await this.update_status_monit_event(connection);
	}

	/**
	 * Get ProcInfo object ready to send to client
	 */
	get_app_info(): schema.ProcInfo {
		return Object.assign({
			db_id: this.id,
			commit: this.deploy.get_commit_info()
		}, this.info);
	}

	/**
	 * Save process to db (without monit info)
	 */
	save(connection?: DatabaseConnection): Promise<void> {
		this.log_header("Saving process info to DB");

		return this.database.run_with_connection((connection) => {
			return connection.process_save({
				ProcessID: this.id,
				Name: this.name,
				ExecPath: this.exec,
				Directory: this.dir,
				Config: JSON.stringify(this.config),
				DeployMethod: this.deploy.method,
				DeploySecret: this.deploy.secret ?? null,
				DeployBranch: this.deploy.branch ?? null,
				DeployUseSSH: this.deploy.use_ssh == true ? 1 : 0,
				DeployPrivateKey: this.deploy.private_key?.toBuffer('rfc4253') ?? null,
				DeployBuildStage: this.deploy.build_stage ?? null,
				LogCustomTypes: this.log_custom_types != null ? JSON.stringify(this.log_custom_types) : null
			});
		}, connection);
	}

	static async load_all(database: Database, connection?: DatabaseConnection): Promise<Process[]> {
		return database.run_with_connection(async (connection) => {
			const output: Process[] = [];
			(await connection.process_load_all()).forEach((row: DatabaseProcess) => {
				output.push(new Process(row, database));
			});

			return output;
		}, connection);
	}

	/**
	 * Compute average values and save values and events to db
	 * TODO: Custom data
	 */
	async rotate_monit_log(connection?: DatabaseConnection): Promise<void> {
		return this.database.run_with_connection(async (connection) => {
			const time = Date.now();

			try {
				if (this.monit_sum_count != 0) {
					const monit_data = new MonitData(this.id, time / 1000, this.cpu_sum / this.monit_sum_count, this.memory_sum / this.monit_sum_count, {});
					await monit_data.save(connection);
				}

				this.cpu_sum = 0;
				this.memory_sum = 0;
				this.monit_sum_count = 0;
			} catch (e) {
				throw new ApiError(500, "Cannot write monit logs to database", e);
			}
		}, connection);
	}

	/**
	 * Install node modules dependencies via yarn
	 */
	install_yarn(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.info.status = schema.ProcStatus.updating;

			this.log_header("Updating dependencies with Yarn");

			const yarn = UnixUser.run_as_specific(() => spawn(Process.yarn_exec + " install --inline-builds --json", {
				cwd: this.dir,
				stdio: ['ignore', 'pipe', 'pipe'],
				shell: true,
				env: Process.yarn_env
			}), this.config.uid, this.config.gid);

			yarn.on('error', (err) => {
				this.pm2m_log.error(err);
			});

			readline.createInterface({ input: yarn.stdout, terminal: false }).on('line', (line) => {
				try {
					const yarn_line: YarnJson = JSON.parse(line);
					const log_call = yarn_line.type == 'warning' ? this.pm2m_log.warn : this.pm2m_log.log;

					log_call(yarn_line.displayName + " " + yarn_line.indent + yarn_line.data);
				} catch (_e) {
					this.pm2m_log.log(line);
				}
			});

			readline.createInterface({ input: yarn.stderr, terminal: false }).on('line', (line) => {
				try {
					const yarn_line: YarnJson = JSON.parse(line);
					this.pm2m_log.error(yarn_line.displayName + " " + yarn_line.indent + yarn_line.data);
				} catch (_e) {
					this.pm2m_log.error(line);
				}
			});

			yarn.on('close', (code) => {
				this.info.status = schema.ProcStatus.stopped;
				if (code == 0)
					resolve();
				else
					reject(new ApiError(500, "Yarn install failed"));
			});
		});
	}

	/**
	 * Build process project
	 */
	async build(): Promise<void> {
		const build_stage = this.deploy.build_stage;
		if (build_stage == null)
			throw new ApiError(400, "Process doesn't have build stage");

		const stopped = this.is_normal_stopped();
		if (!stopped)
			await this.stop();

		await this.install_yarn();

		this.log_header("Building project");
		this.info.status == schema.ProcStatus.updating;

		await new Promise<void>((resolve, reject) => {
			const yarn = UnixUser.run_as_specific(() => spawn(Process.yarn_exec, ["run", build_stage], {
				cwd: this.dir,
				stdio: ['ignore', 'pipe', 'pipe'],
				shell: true,
				env: Process.yarn_env
			}), this.config.uid, this.config.gid);

			yarn.on('error', (error) => { this.pm2m_log.error(error); });

			readline.createInterface({ input: yarn.stdout, terminal: false }).on('line', (line) => { this.pm2m_log.log(line); });
			readline.createInterface({ input: yarn.stderr, terminal: false }).on('line', (line) => { this.pm2m_log.error(line); });

			yarn.on('close', (code) => {
				this.info.status = schema.ProcStatus.stopped;
				if (code == 0)
					resolve();
				else
					reject(new ApiError(500, "Build failed"));
			});
		});

		if (!stopped)
			await this.start();
	}

	/**
	 * Start a process (and install yarn before)
	 */
	async start(run_yarn = true): Promise<void> {
		if (!this.is_normal_stopped())
			await this.stop();

		if (run_yarn)
			await this.install_yarn();

		let node_options = "";

		try {
			const pnp_path = path.join(this.dir, '.pnp.cjs');
			await access_promise(pnp_path);
			node_options = '--require "' + path.join(this.dir, '.pnp.cjs').replaceAll('"', '\\"') + '"';

			const loader_path = path.join(this.dir, ".pnp.loader.mjs");
			await access_promise(loader_path);
			node_options += ' --loader "' + loader_path.replaceAll('"', '\\"') + '"';
		} catch (_e) {
			// module loader doesn't exists, but it's not necessary
		}

		const start_args = {
			...this.config,
			name: this.name,
			cwd: this.dir,
			env: {
				...this.config.env,
				NODE_OPTIONS: node_options
			}
		};

		this.log_header("Starting process");
		this.pm2m_log.log(this.exec);
		this.pm2m_log.log(start_args);

		const proc = await PM2.start(this.exec, start_args);
		this.info.pm2_id = proc.pm_id;

		await this.clean_bad_pm2_processes();
	}

	/**
	 * Stop a process
	 */
	async stop(): Promise<void> {
		if (this.info.pm2_id != null) {
			this.log_header("Stopping process");
			await PM2.stop(this.info.pm2_id);
		}
		else
			throw new ApiError(400, "Stop on non spawned process");
	}

	/**
	 * Convert relative path to absolute and resolve all links
	 * @param switch_user true if user/group will be switched for `fs` call
	 * @returns absolute path
	 */
	resolve_path(pathrel: string, switch_user = true): ResolvedRealPath {
		let pathabs = path.normalize(path.join(this.dir, pathrel));
		let exists = true;
		let access = true;

		try {
			const uid = switch_user ? this.config.uid : undefined;
			const gid = switch_user ? this.config.gid : undefined;
			pathabs = UnixUser.run_as_specific(() => fs.realpathSync(pathabs), uid, gid);
		} catch (e) {
			const error = e as NodeJS.ErrnoException; // realpath throws a syscall error

			exists = false;
			if (error.code == null || error.code == 'EACCES')
				access = false;
		}

		if (!pathabs.startsWith(this.dir))
			throw new ApiError(403, "Requesting path that is outside process directory");

		return {
			path: pathabs,
			exists: exists,
			access: access
		};
	}

	private create_path_info_base(stats: fs.Stats, final_path: string): schema.PathInfoBase {
		const type_value = ((stats.mode & 0o170000) / 0o10000);
		let type = schema.PathType.unknown;
		if (type_value in schema.PathType)
			type = type_value as schema.PathType;

		return {
			path: final_path,
			type: type,
			uid: stats.uid,
			gid: stats.gid,
			permission: stats.mode & 0o777,
			size: stats.size,
			create_time: stats.ctimeMs / 1000,
			modify_time: stats.mtimeMs / 1000
		};
	}

	/**
	 * List all entries in directory.
	 * User/group will be switched for `fs` calls.
	 * @param pathrel relative path from project root
	 */
	list_directory(pathrel: string): schema.PathInfo[] {
		return UnixUser.run_as_specific(() => {
			const resolved_path = this.resolve_path(pathrel, false);
			if (!resolved_path.access)
				throw new ApiError(403, "Forbidden access to list a directory");
			if (!resolved_path.exists)
				throw new ApiError(404, "Directory not exists");

			return fs.readdirSync(resolved_path.path).map(file => {
				const fileabs = path.join(resolved_path.path, file);
				const stats = fs.lstatSync(fileabs);

				let symlink: schema.PathInfoBase | null = null;
				if (stats.isSymbolicLink()) {
					const destination = fs.readlinkSync(fileabs);
					const destination_abs = path.isAbsolute(destination) ? destination : path.join(resolved_path.path, destination);

					try {
						if (!destination_abs.startsWith(this.dir))
							throw new Error("Symlink leads outside of process directory");

						symlink = this.create_path_info_base(fs.lstatSync(destination_abs), destination);
					} catch (e: unknown) {
						new ApiError(403, "Invalid symlink destination", e).log_error();

						symlink = {
							path: destination,
							type: schema.PathType.unknown,
							uid: -1,
							gid: -1,
							permission: 0,
							size: -1,
							create_time: -1,
							modify_time: -1
						};
					}
				}

				return {
					...this.create_path_info_base(stats, path.relative(this.dir, fileabs)),
					symlink
				};
			});
		}, this.config.uid, this.config.gid);
	}

	/**
	 * Create new directory.
	 * User/group will be switched for `fs` calls.
	 * @param pathrel relative path from project root
	 * @param recursive true if parent directories will be created too if not exists
	 */
	create_directory(pathrel: string, recursive?: boolean): void {
		UnixUser.run_as_specific(() => {
			const resolved_path = this.resolve_path(pathrel, false);
			if (!resolved_path.access)
				throw new ApiError(403, "Forbidden access to parent directory");
			if (resolved_path.exists)
				throw new ApiError(400, "Directory already exists");

			if (recursive == false) {
				const parent_resolved = this.resolve_path(path.dirname(resolved_path.path), false);
				if (!parent_resolved.exists)
					throw new ApiError(400, "Parent directory doesn't exists");
			}

			fs.mkdirSync(resolved_path.path, { recursive: recursive });
		}, this.config.uid, this.config.gid);
	}

	/**
	 * Move file/directory to new location.
	 * User/group will be switched for `fs` calls.
	 * @param source relative path from project root
	 * @param target relative path from project root
	 */
	move_path(source: string, target: string): void {
		UnixUser.run_as_specific(() => {
			const resolved_source = this.resolve_path(source, false);
			const resolved_target = this.resolve_path(target, false);

			if (!resolved_source.exists)
				throw new ApiError(400, "Source path doesn't exists");

			if (!resolved_target.access)
				throw new ApiError(403, "Target path is not accessable");
			if (resolved_target.exists)
				throw new ApiError(400, "Target path already exists");

			fs.renameSync(resolved_source.path, resolved_target.path);
		}, this.config.uid, this.config.gid);
	}

	/**
	 * Remove file/directory.
	 * User/group will be switched for `fs` calls.
	 * @param pathrel relative path from project root
	 * @param recursive true if children entries will be removed too. Must be true if `pathrel` is directory.
	 */
	remove_path(pathrel: string, recursive?: boolean): void {
		UnixUser.run_as_specific(() => {
			const resolved_path = this.resolve_path(pathrel, false);
			if (!resolved_path.exists)
				throw new ApiError(400, "Path to remove doesn't exists");

			if (recursive == false && fs.lstatSync(resolved_path.path).isDirectory())
				throw new ApiError(400, "Cannot remove directory without 'recursive' parameter");

			fs.rmSync(resolved_path.path, { recursive: recursive });
		}, this.config.uid, this.config.gid);
	}

	/**
	 * Open file.
	 * User/group will be switched for `fs` calls.
	 * @param pathrel relative path from project root
	 * @param write true to open file in rw mode (i.e. 'a+')
	 * @returns object with file descriptor and resolved relative path
	 */
	open_file(pathrel: string, write: boolean): OpenFileData {
		return UnixUser.run_as_specific(() => {
			const resolved_path = this.resolve_path(pathrel, false);
			if (!resolved_path.access)
				throw new ApiError(403, "Requested file is not accessable");

			if (!write && !resolved_path.exists)
				throw new ApiError(400, "Requested file doesn't exists");

			if (resolved_path.exists && !fs.statSync(resolved_path.path).isFile())
				throw new ApiError(400, "Cannot open other entry types then file");

			try {
				const fd = fs.openSync(resolved_path.path, write ? (fs.constants.O_RDWR | fs.constants.O_CREAT) : (fs.constants.O_RDONLY), 0o644);
				return { fd: fd, path: path.relative(this.dir, resolved_path.path) };
			} catch (e) {
				throw new ApiError(403, "Forbidden access to open file", e);
			}
		}, this.config.uid, this.config.gid);
	}

	/**
	 * Open file without resolving path and checking file existence.
	 * User/group will be switched for `fs` calls.
	 * @param pathabs absolute path to file
	 * @param write true to open file in rw mode (i.e. 'a+')
	 * @param uid user id for 'fs' call, if its not defined, user is not switched
	 * @param gid group id for 'fs' call, if its not defined, user is not switched
	 * @returns object with file descriptor and resolved relative path
	 */
	open_file_unsafe(pathabs: string, write: boolean, uid: number | undefined, gid: number | undefined): OpenFileData {
		return UnixUser.run_as_specific(() => {
			try {
				const fd = fs.openSync(pathabs, write ? 'a+' : 'r');
				return { fd: fd, path: pathabs };
			} catch (e) {
				throw new ApiError(403, "Forbidden access to open file", e);
			}
		}, uid, gid);
	}

	/**
	 * Close opened file.
	 * @returns true if file was watched and this watching is stopped too
	 */
	async close_file(data: OpenFileData): Promise<boolean> {
		const watched = data.watcher != null;
		if (watched)
			this.unwatch_file(data);
		await close_promise(data.fd);
		return watched;
	}

	/**
	 * Start watching a file without resolving path and checking file existence.
	 * User/group will be switched for `fs` calls.
	 * @param pathabs absolute path to file
	 * @param switch_user false if watching is called in privileged mode
	 * @param event callback from `fs.watch`
	 */
	watch_file_unsafe(pathabs: string, switch_user: boolean, event: fs.WatchListener<string>): fs.FSWatcher {
		const uid = switch_user ? this.config.uid : undefined;
		const gid = switch_user ? this.config.gid : undefined;

		return UnixUser.run_as_specific(() => {
			return fs.watch(pathabs, event);
		}, uid, gid);
	}

	/**
	 * Stop watching a file
	 */
	unwatch_file(data: OpenFileData): void {
		data.watcher?.close();
		data.watcher = undefined;
		if (data.watcher_close_callback != null)
			data.watcher_close_callback();
		data.watcher_close_callback = undefined;
	}
}
