import mysql from 'promise-mysql';
import * as schema from 'pm2manager_schema';
import { ApiError } from './api_error';
import { ConfigLoader } from './config';

export const DatabaseVersion = 1;

export type NumberBoolean = 0 | 1;

export interface DatabaseInfo {
	DbVersion: number;
}

export interface DatabaseUser {
	UserID: number;
	Username: string;
	Password: string;
	/** Unix timestamp */
	LastLogin: number;
	Admin: NumberBoolean;
}

export interface DatabaseToken {
	TokenID: number;
	UserID: number;
	/** Unix timestamp */
	ValidUntil: number;
	Token: string;
	IP: string;
}

export interface DatabaseUserPerm {
	PermissionID: number;
	UserID: number;
	ProcessID: number;
	View: NumberBoolean;
	Env: NumberBoolean;
	FileRead: NumberBoolean;
	FileWrite: NumberBoolean;
	Manage: NumberBoolean;
	Log: NumberBoolean;
	Deploy: NumberBoolean;
}

export interface DatabaseProcess {
	ProcessID: number;
	Name: string;
	ExecPath: string;
	Directory: string;
	Config: string;
	DeployMethod: schema.DeployMethod;
	DeploySecret: string | null;
	DeployBranch: string | null;
	DeployUseSSH: NumberBoolean;
	DeployPrivateKey: Buffer | null;
	DeployBuildStage: string | null;
	LogCustomTypes: string | null;
}

export interface DatabaseMonitData {
	MonitDataID: number;
	ProcessID: number;
	/** Unix timestamp */
	Time: number;
	CPU: number;
	Memory: number;
	CustomData: string | null;
}

export interface DatabaseMonitEvent {
	MonitEventID: number;
	ProcessID: number;
	/** Unix timestamp */
	Time: number;
	EventType: schema.ProcStatus;
	Description: string | null;
}

export class DatabaseConnection {
	private connection: mysql.PoolConnection;

	query: mysql.QueryFunction;

	constructor(connection: mysql.PoolConnection) {
		this.connection = connection;
		this.query = connection.query;
	}

	close(): void {
		this.connection.release();
	}

	async transaction_begin(): Promise<void> {
		await this.connection.beginTransaction();
	}

	async transaction_commit(): Promise<void> {
		await this.connection.commit();
	}

	async transaction_rollback(): Promise<void> {
		await this.connection.rollback();
	}

	async user_load_by_token(token: string): Promise<(DatabaseUser & DatabaseToken) | undefined> {
		const results = await this.connection.query("SELECT `users`.*, `tokens`.*, UNIX_TIMESTAMP(`users`.`LastLogin`) AS `LastLogin`, UNIX_TIMESTAMP(`tokens`.`ValidUntil`) AS `ValidUntil` FROM `users`, `tokens` WHERE `tokens`.`Token` = ? AND `tokens`.`UserID` = `users`.`UserID` LIMIT 1", [token]) as Array<DatabaseUser & DatabaseToken>;
		return results.length == 1 ? results[0] : undefined;
	}

	async user_load_by_name(username: string): Promise<DatabaseUser | undefined> {
		const results = await this.connection.query("SELECT *, UNIX_TIMESTAMP(`LastLogin`) AS `LastLogin` FROM `users` WHERE `Username` = ? LIMIT 1", [username]) as DatabaseUser[];
		return results.length == 1 ? results[0] : undefined;
	}

	async user_update_last_login(user_id: number, last_login: number): Promise<void> {
		await this.connection.query("UPDATE `users` SET `LastLogin` = FROM_UNIXTIME(?) WHERE `UserID` = ?", [last_login, user_id]);
	}

	async user_update_full(data: DatabaseUser): Promise<void> {
		await this.connection.query("UPDATE `users` SET `Username` = ?, `Password` = ?, `LastLogin` = FROM_UNIXTIME(?), `Admin` = ? WHERE `UserID` = ?", [data.Username, data.Password, data.LastLogin, data.Admin, data.UserID]);
	}

	async token_insert(data: Omit<DatabaseToken, "TokenID">): Promise<number> {
		const results = await this.connection.query("INSERT INTO `tokens` (`UserID`, `Token`, `ValidUntil`, `IP`) VALUES (?, ?, FROM_UNIXTIME(?), ?)", [data.UserID, data.Token, data.ValidUntil, data.IP]);
		return results.insertId;
	}

	async user_perm_load_specific(user_id: number, process_id: number): Promise<DatabaseUserPerm | undefined> {
		const results = await this.connection.query("SELECT * FROM `users_process_perm` WHERE `UserID` = ? AND `ProcessID` = ? LIMIT 1", [user_id, process_id]) as DatabaseUserPerm[];
		return results.length == 1 ? results[0] : undefined;
	}

	async user_perm_load_by_user(user_id: number): Promise<DatabaseUserPerm[]> {
		return await this.connection.query("SELECT * FROM `users_process_perm` WHERE `UserID` = ?", [user_id]) as DatabaseUserPerm[];
	}

	async process_load_specific(process_id: number): Promise<DatabaseProcess | undefined> {
		const results = await this.connection.query("SELECT * FROM `processes` WHERE `ProcessID` = ? LIMIT 1", [process_id]) as DatabaseProcess[];
		return results.length == 1 ? results[0] : undefined;
	}

	async process_load_all(): Promise<DatabaseProcess[]> {
		return await this.connection.query("SELECT * FROM `processes`") as DatabaseProcess[];
	}

	async process_save(data: DatabaseProcess): Promise<void> {
		await this.connection.query("UPDATE `processes` SET `Name` = ?, `ExecPath` = ?, `Directory` = ?, `Config` = ?, `DeployMethod` = ?, `DeploySecret` = ?, `DeployBranch` = ?, `DeployUseSSH` = ?, `DeployPrivateKey` = ?, `LogCustomTypes` = ? WHERE `ProcessID` = ?", [data.Name, data.ExecPath, data.Directory, data.Config, data.DeployMethod, data.DeploySecret, data.DeployBranch, data.DeployUseSSH, data.DeployPrivateKey, data.LogCustomTypes, data.ProcessID]);
	}

	async monit_data_load_specific(monit_data_id: number): Promise<DatabaseMonitData | undefined> {
		const results = await this.connection.query("SELECT *, UNIX_TIMESTAMP(`Time`) AD `Time` FROM `monit_data` WHERE `MonitDataID` = ? LIMIT 1", [monit_data_id]) as DatabaseMonitData[];
		return results.length == 1 ? results[0] : undefined;
	}

	async monit_data_load_range(process_id: number, start_time: number, end_time: number): Promise<DatabaseMonitData[]> {
		return await this.connection.query("SELECT *, UNIX_TIMESTAMP(`Time`) AS `Time` FROM `monit_data` WHERE `ProcessID` = ? AND `Time` BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?)", [process_id, start_time, end_time]) as DatabaseMonitData[];
	}

	async monit_data_insert(data: Omit<DatabaseMonitData, "MonitDataID">): Promise<number> {
		const results = await this.connection.query("INSERT INTO `monit_data` (`ProcessID`, `Time`, `CPU`, `Memory`, `CustomData`) VALUES (?, FROM_UNIXTIME(?), ?, ?, ?)", [data.ProcessID, data.Time, data.CPU, data.Memory, data.CustomData]);
		return results.insertId;
	}

	async monit_data_update(data: DatabaseMonitData): Promise<void> {
		await this.connection.query("UPDATE `monit_data` SET `ProcessID` = ?, `Time` = ?, `CPU` = ?, `Memory` = ?, `CustomData` = ? WHERE `MonitEventID` = ?", [data.ProcessID, data.Time, data.CPU, data.Memory, data.CustomData, data.MonitDataID]);
	}

	async monit_event_load_specific(monit_event_id: number): Promise<DatabaseMonitEvent | undefined> {
		const results = await this.connection.query("SELECT *, UNIX_TIMESTAMP(`Time`) AS `Time` FROM `monit_events` WHERE `MonitEventID` = ? LIMIT 1", [monit_event_id]) as DatabaseMonitEvent[];
		return results.length == 1 ? results[0] : undefined;
	}

	async monit_event_load_last(process_id: number): Promise<DatabaseMonitEvent | undefined> {
		const results = await this.connection.query("SELECT *, UNIX_TIMESTAMP(`Time`) AS `Time` FROM `monit_events` WHERE `ProcessID` = ? ORDER BY `Time` DESC LIMIT 1", [process_id]) as DatabaseMonitEvent[];
		return results.length == 1 ? results[0] : undefined;
	}

	async monit_event_load_range(process_id: number, start_time: number, end_time: number): Promise<DatabaseMonitEvent[]> {
		return await this.connection.query("SELECT *, UNIX_TIMESTAMP(`Time`) AS `Time` FROM `monit_events` WHERE `ProcessID` = ? AND `Time` BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?)", [process_id, start_time, end_time]) as DatabaseMonitEvent[];
	}

	async monit_event_insert(data: Omit<DatabaseMonitEvent, "MonitEventID">): Promise<number> {
		const results = await this.connection.query("INSERT INTO `monit_events` (`ProcessID`, `Time`, `EventType`, `Description`) VALUES (?, FROM_UNIXTIME(?), ?, ?)", [data.ProcessID, data.Time, data.EventType, data.Description]);
		return results.insertId;
	}

	async monit_event_update(data: DatabaseMonitEvent): Promise<void> {
		await this.connection.query("UPDATE `monit_events` SET `ProcessID` = ?, `Time` = ?, `EventType` = ?, `Description` = ? WHERE `MonitEventID` = ?", [data.ProcessID, data.Time, data.EventType, data.Description, data.MonitEventID]);
	}

	async info_select(): Promise<DatabaseInfo | undefined> {
		const results = await this.connection.query("SELECT * FROM `info` LIMIT 1") as DatabaseInfo[];
		return results.length == 1 ? results[0] : undefined;
	}
}

export class Database {
	db_pool: mysql.Pool | null;

	constructor() {
		this.db_pool = null;
	}

	async connect(): Promise<void> {
		const config = ConfigLoader.config;

		this.db_pool = await mysql.createPool({
			host: config.mysql.host,
			port: config.mysql.port,
			user: config.mysql.user,
			password: config.mysql.password == "" ? undefined : config.mysql.password,
			database: config.mysql.database
		});
	}

	check_db_version(): Promise<void> {
		return this.run_with_connection(async (connection) => {
			const info = await connection.info_select();
			if (info?.DbVersion != DatabaseVersion)
				throw new Error(`Database have invalid version ${info?.DbVersion}, expected ${DatabaseVersion}`);
		});
	}

	clear_expired_tokens(): Promise<void> {
		console.log("Cleaning expired tokens");
		return this.run_with_connection(async (connection) => {
			await connection.query("DELETE FROM `tokens` WHERE `ValidUntil` < NOW()");
		});
	}

	async close(): Promise<void> {
		if (this.db_pool == null)
			return;

		this.db_pool.end();
	}

	async get_connection(): Promise<DatabaseConnection> {
		if (this.db_pool == null)
			throw new ApiError(500, "Trying to get connection from non initialized database");

		const connection = await this.db_pool.getConnection();
		return new DatabaseConnection(connection);
	}

	/**
	 * Run function with valid database connection.
	 * Use existing connection from `connection` argument if exists or create one for this call.
	 * 
	 * Newly created connection is closed after call and is no need to close.
	 */
	async run_with_connection<T>(call: (connection: DatabaseConnection) => Promise<T>, connection?: DatabaseConnection): Promise<T> {
		if (connection == null) {
			connection = await this.get_connection();
			try {
				return await call(connection);
			} finally {
				connection.close();
			}
		} else
			return call(connection);
	}
}
