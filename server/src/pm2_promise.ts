/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import pm2 from 'pm2';

export { ProcessDescription, StartOptions } from 'pm2';

/**
 * pm2 wrapper to promise-like functions
 * 
 * @static
 */
export class PM2 {
	/**
	 * Either connects to a running pm2 daemon (“God”) or launches and daemonizes one.
	 * Once launched, the pm2 process will keep running after the script exits.
	 * 
	 * @param noDaemonMode (default false) If true is passed for the first argument
	 * pm2 will not be run as a daemon and will die when the related script exits.
	 * By default, pm2 stays alive after your script exits.
	 * If pm2 is already running, your script will link to the existing daemon but will die once your process exits.
	 */
	static connect(noDaemonMode?: boolean): Promise<void> {
		return new Promise((resolve, reject) => {
			pm2.connect(noDaemonMode === true, (err) => {
				if (err)
					reject(err);
				else
					resolve();
			});
		});
	}

	/**
	 * Disconnects from the pm2 daemon.
	 */
	static disconnect(): void {
		pm2.disconnect();
	}

	/**
	 * Starts a script that will be managed by pm2.
	 * 
	 * @param script The path of the script to run.
	 * @param options Options
	 */
	static start(script: string, options: pm2.StartOptions): Promise<pm2.Proc> {
		return new Promise((resolve, reject) => {
			pm2.start(script, options, (err, proc) => {
				if (err)
					reject(err);
				else
					resolve(proc);
			});
		});
	}

	/**
	 * Stops a process but leaves the process meta-data in pm2’s list
	 * 
	 * @param process Can either be the name as given in the pm2.start options,
	 * a process id, or the string “all” to indicate that all scripts should be restarted.
	 */
	static stop(process: string | number): Promise<pm2.Proc> {
		return new Promise((resolve, reject) => {
			pm2.stop(process, (err, proc) => {
				if (err)
					reject(err);
				else
					resolve(proc);
			});
		});
	}

	/**
	 * Stops and restarts the process.
	 * 
	 * @param process Can either be the name as given in the pm2.start options,
	 * a process id, or the string “all” to indicate that all scripts should be restarted.
	 */
	static restart(process: string | number): Promise<pm2.Proc> {
		return new Promise((resolve, reject) => {
			pm2.restart(process, (err, proc) => {
				if (err)
					reject(err);
				else
					resolve(proc);
			});
		});
	}
	/**
	 * Zero-downtime rolling restart. At least one process will be kept running at
	 * all times as each instance is restarted individually.
	 * Only works for scripts started in cluster mode.
	 * 
	 * @param process Can either be the name as given in the pm2.start options,
	 * a process id, or the string “all” to indicate that all scripts should be restarted.
	 * @param options - An object containing configuration
	 * @param options.updateEnv - (Default: false) If true is passed in, pm2 will reload it’s
	 * environment from process.env before reloading your process.
	 */
	static reload(process: string | number, options: { updateEnv: boolean }): Promise<pm2.Proc> {
		return new Promise((resolve, reject) => {
			pm2.reload(process, options, (err, proc) => {
				if (err)
					reject(err);
				else
					resolve(proc);
			});
		});
	}

	/**
	 * Stops the process and removes it from pm2’s list.
	 * The process will no longer be accessible by its name
	 * 
	 * @param process Can either be the name as given in the pm2.start options,
	 * a process id, or the string “all” to indicate that all scripts should be restarted.
	 */
	static delete(process: string | number): Promise<pm2.Proc> {
		return new Promise((resolve, reject) => {
			pm2.delete(process, (err, proc) => {
				if (err)
					reject(err);
				else
					resolve(proc);
			});
		});
	}

	/**
	 * Kills the pm2 daemon (same as pm2 kill). Note that when the daemon is killed, all its
	 * processes are also killed. Also note that you still have to explicitly disconnect
	 * from the daemon even after you kill it.
	 */
	static killDaemon(): Promise<pm2.ProcessDescription> {
		return new Promise((resolve, reject) => {
			pm2.killDaemon((err, process_description) => {
				if (err)
					reject(err);
				else
					resolve(process_description);
			});
		});
	}

	/**
	 * Returns various information about a process: eg what stdout/stderr and pid files are used.
	 * 
	 * @param process - Can either be the name as given in the pm2.start options,
	 * a process id, or the string “all” to indicate that all scripts should be restarted.
	 */
	static describe(process: string | number): Promise<pm2.ProcessDescription[]> {
		return new Promise((resolve, reject) => {
			pm2.describe(process, (err, process_descriptions) => {
				if (err)
					reject(err);
				else
					resolve(process_descriptions);
			});
		});
	}

	/**
	 * Gets the list of running processes being managed by pm2.
	 */
	static list(): Promise<pm2.ProcessDescription[]> {
		return new Promise((resolve, reject) => {
			pm2.list((err, process_descriptions) => {
				if (err)
					reject(err);
				else
					resolve(process_descriptions);
			});
		});
	}

	/**
	 * Writes the process list to a json file at the path in the DUMP_FILE_PATH environment variable
	 * (“~/.pm2/dump.pm2” by default).
	 */
	static dump(): Promise<any> {
		return new Promise((resolve, reject) => {
			pm2.dump((err, result) => {
				if (err)
					reject(err);
				else
					resolve(result);
			});
		});
	}

	/**
	 * Flushes the logs.
	 * 
	 * @param {number|string} process Can either be the name as given in the pm2.start options,
	 * a process id, or the string “all” to indicate that all scripts should be restarted.
	 */
	static flush(process: number | string): Promise<any> {
		return new Promise((resolve, reject) => {
			pm2.flush(process, (err, result) => {
				if (err)
					reject(err);
				else
					resolve(result);
			});
		});
	}

	/**
	 * Rotates the log files. The new log file will have a higher number
	 * in it (the default format being ${process.name}-${out|err}-${number}.log).
	 */
	static reloadLogs(): Promise<any> {
		return new Promise((resolve, reject) => {
			pm2.reloadLogs((err, result) => {
				if (err)
					reject(err);
				else
					resolve(result);
			});
		});
	}

	/**
	 * Opens a message bus.
	 */
	static launchBus(): Promise<any> {
		return new Promise((resolve, reject) => {
			pm2.launchBus((err, bus) => {
				if (err)
					reject(err);
				else
					resolve(bus);
			});
		});
	}

	/**
	 * @param process Can either be the name as given in the pm2.start options,
	 * a process id, or the string “all” to indicate that all scripts should be restarted.
	 */
	static sendSignalToProcessName(signal: string | number, process: string | number): Promise<any> {
		return new Promise((resolve, reject) => {
			pm2.sendSignalToProcessName(signal, process, (err, result) => {
				if (err)
					reject(err);
				else
					resolve(result);
			});
		});
	}

	/**
	 * Registers the script as a process that will start on machine boot. The current process list will be dumped and saved for resurrection on reboot.
	 */
	static startup(platform: 'ubuntu' | 'centos' | 'redhat' | 'gentoo' | 'systemd' | 'darwin' | 'amazon'): Promise<any> {
		return new Promise((resolve, reject) => {
			pm2.startup(platform, (err, result) => {
				if (err)
					reject(err);
				else
					resolve(result);
			});
		});
	}
}
