import cryptoRandomString from 'crypto-random-string';
import safe_compare from 'safe-compare';
import crypto from 'crypto';
import { Process } from './process';
import nodegit from 'nodegit';
import { PrivateKey } from 'sshpk';
import { PushEvent } from '@octokit/webhooks-types';
import { DatabaseProcess } from './database';
import * as schema from 'pm2manager_schema';

interface DeployData {
	push: boolean;
	commit?: string;
	ssh_url?: string;
	http_url?: string;
	github_deploy_url?: string;
}

export class Deploy {
	static readonly SECRET_LENGTH = 16;
	static readonly SUPPORTED_SECRET_HASHES = ['sha1', 'sha256', 'sha512'];

	process: Process;
	method: schema.DeployMethod;
	branch?: string;

	secret?: string;
	use_ssh?: boolean;
	private_key?: PrivateKey;

	build_stage?: string;

	repository?: nodegit.Repository;
	commit?: nodegit.Commit;

	constructor(process: Process, data: DatabaseProcess) {
		this.process = process;
		this.method = data.DeployMethod;
		this.branch = data.DeployBranch ?? undefined;
		this.secret = data.DeploySecret ?? undefined;
		this.use_ssh = data.DeployUseSSH === 1;
		this.private_key = data.DeployPrivateKey ? PrivateKey.parse(data.DeployPrivateKey, 'rfc4253') : undefined;
		this.build_stage = data.DeployBuildStage ?? undefined;
	}

	/** Open process repository */
	async open_repository(): Promise<void> {
		if (this.repository) //already opened
			return;

		try {
			this.repository = await nodegit.Repository.open(this.process.dir);
		} catch (reason) {
			this.repository = undefined;
		}
	}

	get_deploy_data(): schema.ResponseDeployData {
		return {
			process_id: this.process.id,
			method: this.method,
			branch: this.branch,
			secret: this.secret,
			use_ssh: this.use_ssh,
			ssh_key: this.private_key?.toPublic().toString('ssh'),
			build_stage: this.build_stage
		};
	}

	set_deploy_data(data: schema.RequestSetDeployData): void {
		if (data.method !== undefined)
			this.method = data.method;
		if (data.branch !== undefined)
			this.branch = data.branch;
		if (data.use_ssh !== undefined)
			this.use_ssh = data.use_ssh;
		if (data.build_stage !== undefined)
			this.build_stage = data.build_stage ?? undefined;
	}

	/** Get current commit info (last update from update_commit_info) */
	get_commit_info(): schema.CommitInfo | undefined {
		if (this.commit) {
			const author = this.commit.author();

			return {
				summary: this.commit.summary(),
				sha1: this.commit.sha(),
				author: {
					name: author.name(),
					email: author.email()
				},
				time: this.commit.timeMs()
			};
		}

		return undefined;
	}

	/**
	 * Update commit info (get current head commit)
	 */
	async update_commit_info(): Promise<void> {
		await this.open_repository();
		if (!this.repository) {
			this.commit = undefined;
			return;
		}

		this.commit = await this.repository.getHeadCommit();
	}

	/**
	 * Generate new secret key and return it
	 */
	generate_secret(): string {
		this.secret = cryptoRandomString({ length: Deploy.SECRET_LENGTH, type: "alphanumeric" });
		return this.secret;
	}

	generate_private_key(): void {
		this.private_key = PrivateKey.generate('ed25519');
	}

	get_public_key(): string | undefined {
		return this.private_key?.toPublic().toString('ssh');
	}

	/**
	 * Validate signature of request
	 */
	validate_sign(raw_data: Buffer, signature?: string): boolean {
		if (this.secret == null || signature == null)
			return false;

		const splitted = signature.split("=");
		if (splitted.length != 2 || !Deploy.SUPPORTED_SECRET_HASHES.includes(splitted[0]))
			return false;

		const hmac = crypto.createHmac(splitted[0], this.secret);
		hmac.update(raw_data);
		const computed_signature = hmac.digest('hex');

		const retval = safe_compare(splitted[1], computed_signature);
		return retval;
	}

	parse_github(event: string, data: PushEvent): DeployData | null {
		try {
			if (event === 'ping')
				return { push: false };
			if (event !== 'push' || data == null)
				return null;

			if (this.branch == null || data.ref != "refs/heads/" + this.branch)
				return { push: false };

			return {
				push: true,
				commit: data.after,
				ssh_url: data.repository.ssh_url,
				http_url: data.repository.clone_url,
				github_deploy_url: data.repository.deployments_url
			};
		} catch (e) { // Catch probably wrong input object
			return null;
		}
	}

	parse_api(event: string, data: any): DeployData | null {
		throw new Error("parse_api not implemented yet.");
	}

	/**
	 * Promise is resolved after process is stopped.
	 * Implements busy waiting with periodic setTimeout call.
	 */
	private wait_until_stop(): Promise<void> {
		return new Promise((resolve) => {
			const wait = (): void => {
				if (this.process.is_stopped())
					resolve();
				else
					setTimeout(wait, 200);
			};

			wait();
		});
	}

	/**
	 * Execute a deploy
	 * TODO: github deployment
	 * 
	 * If deploy failed, log error to process pm2m log and forwards the error
	 */
	async run(deploy_data: DeployData): Promise<void> {
		if (!deploy_data.push)
			return;

		this.process.log_header("Starting deploy process");
		this.process.pm2m_log.log(deploy_data);

		await this.open_repository();

		let remote_url = deploy_data.http_url;
		let fetch_options: nodegit.FetchOptions | undefined = undefined;

		if (this.use_ssh === true) {
			remote_url = deploy_data.ssh_url;
			if (!this.private_key)
				throw new Error("Private key doesn't exists!");

			const private_key = this.private_key.toString('ssh');
			const public_key = this.private_key.toPublic().toString('ssh');

			fetch_options = {
				callbacks: {
					credentials: (url: string, username: string): Promise<nodegit.Cred> => {
						return nodegit.Cred.sshKeyMemoryNew(username, public_key, private_key, "");
					}
				}
			};
		}

		try {
			if (!this.repository) // git repo is not initialized
				this.repository = await nodegit.Repository.init(this.process.dir, 0);

			if (remote_url == null)
				throw new Error("Invalid remote url");

			// check if process is regularly stopped (if errored, start it after deploy)
			const stopped = this.process.is_normal_stopped();
			if (!stopped)
				await this.process.stop();

			const remote = await nodegit.Remote.createAnonymous(this.repository, remote_url);
			await this.repository.fetch(remote, fetch_options);

			this.commit = await nodegit.Commit.lookup(this.repository, deploy_data.commit as string);

			await this.wait_until_stop();

			await nodegit.Reset.reset(this.repository, this.commit, nodegit.Reset.TYPE.HARD, {});

			if (this.build_stage != null)
				await this.process.build();

			if (!stopped)
				await this.process.start(this.build_stage == null);
		} catch (err) {
			this.process.pm2m_log.error("Deploy failed", err);
			throw err;
		}
	}
}
