import fs from 'fs';
import { ApiServer } from "./api_server";
import { Database } from "./database";
import { DeployServer } from "./deploy_server";
import { ConfigLoader } from './config';
import { PM2 } from "./pm2_promise";
import { ApiError } from "./api_error";
import { Process } from "./process";
import { UnixUser } from "./utils";

export class Main {
	static instance: Main;

	database: Database;
	deploy_server: DeployServer;
	api_server: ApiServer;

	process_map: Map<number, Process>;

	update_interval: NodeJS.Timer;
	last_update_promise?: Promise<void>;
	current_update_remainder: number;

	constructor(database: Database, process_map: Map<number, Process>, deploy_server: DeployServer, api_server: ApiServer) {
		Main.instance = this;

		this.database = database;
		this.deploy_server = deploy_server;
		this.api_server = api_server;

		this.process_map = process_map;

		this.current_update_remainder = 0;
		this.update_interval = setInterval(() => {
			this.last_update_promise = this.update_processes().catch(reason => {
				new ApiError(500, "Update process error", reason).log_error();
			}).then(() => { this.last_update_promise = undefined; });
		}, ConfigLoader.config.main.update_interval);
	}

	static async init(): Promise<Main> {
		const config = ConfigLoader.load('config.json');

		fs.mkdirSync(config.main.process_pm2m_log_directory, { recursive: true });

		await PM2.connect();

		Process.init_yarn();

		const database = new Database();
		await database.connect();
		await database.check_db_version();

		const process_map = await Main.load_processes(database);

		const deploy_server = new DeployServer(config.deploy.port, config.deploy.root_dir, process_map);
		const api_server = new ApiServer(config.api.port, database, process_map, config.api.ping_interval);

		return new Main(database, process_map, deploy_server, api_server);
	}

	private static async load_processes(database: Database): Promise<Map<number, Process>> {
		const connection = await database.get_connection();
		try {
			const processes = await Process.load_all(database, connection);

			const process_map: Map<number, Process> = new Map();
			const promises: Promise<void>[] = [];
			processes.forEach(process => {
				process_map.set(process.id, process);
				promises.push(process.load_last_status(connection, ConfigLoader.config.main.restore_last_status));
			});

			await Promise.all(promises);
			return process_map;
		} finally {
			connection.close();
		}
	}

	async update_processes(force_rotate?: boolean): Promise<void> {
		this.current_update_remainder = (this.current_update_remainder + 1) % ConfigLoader.config.main.rotate_count;
		const rotate = this.current_update_remainder == 0 || force_rotate == true;

		this.api_server.api.update_processes(rotate);
	}

	async stop(): Promise<never> {
		const promises: Promise<void>[] = [];

		clearInterval(this.update_interval);

		promises.push(this.deploy_server.stop());
		promises.push(this.api_server.stop());

		try {
			await Promise.all(promises);
			await this.update_processes(true);
			await this.database.close();
			PM2.disconnect();
		} catch (err) {
			console.error(err);
			process.exit(1);
		}

		process.exit(0);
	}
}
