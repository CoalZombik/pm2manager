import * as schema from "pm2manager_schema";

export class ApiError extends Error {
	status: number;
	parent?: Error;

	constructor(status: number, message: string, parent?: Error | unknown) {
		// Pass message to parent constructor
		super(message);

		this.name = 'ApiError';
		this.status = status;
		if (parent instanceof Error)
			this.parent = parent;
	}

	toString(): string {
		let msg = "[" + new Date().toISOString() + "] ";

		if (this.status == 500 && this.parent) //server side error
			msg += this.parent.stack;
		else
			msg += this.name + " " + this.status + ": " + this.message;

		return msg;
	}

	/** Write error to stderr */
	log_error(): void {
		console.error(this.toString());
	}

	to_response(type?: schema.RequestType, request_id?: number): schema.Response {
		return {
			status: this.status,
			type: type ?? 'error',
			request_id: request_id,
			data: { reason: this.message }
		};
	}
}