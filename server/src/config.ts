import fs from 'fs';
import Ajv from 'ajv';
import { Pm2ManagerConfig as Config } from '../tmp/config';

export class ConfigLoader {
	static config: Config;

	static load(path: string): Config {
		const ajv = new Ajv();

		const schema = JSON.parse(fs.readFileSync("config.schema.json", { encoding: 'utf-8' }));
		const validate = ajv.compile<Config>(schema);

		const config = JSON.parse(fs.readFileSync(path, { encoding: 'utf-8' }));

		if (!validate(config))
			throw Error("'" + path + "' is not valid config file!");

		this.config = config;
		return config as Config;
	}
}