import fs from 'fs';
import util from 'util';
import { Process, MonitData, MonitEvent, OpenFileData } from './process';
import { User } from './user';
import { ApiError } from './api_error';
import { DatabaseConnection, DatabaseUserPerm } from './database';
import * as schema from 'pm2manager_schema';
import path from 'path';
import { ConfigLoader } from './config';

export interface ReadFileChunk {
	start: number,
	length: number,
	buffer: Buffer | null,
	eof: boolean
}

export interface ProcessOpenFile {
	index: number,
	size: number
}

const fstat_promise = util.promisify(fs.fstat);
const read_promise = util.promisify(fs.read);
const write_promise = util.promisify(fs.write);
const ftruncate_promise = util.promisify(fs.ftruncate);

/** Wrapper of Process class with checking of permissions and log reading */
export class UserProcess {
	process: Process;
	user: User;
	view: boolean;
	env: boolean;
	file: { read: boolean, write: boolean };
	manage: boolean;
	log: boolean;
	deploy: boolean;

	opened_files: Map<number, OpenFileData>;
	next_open_id: number;

	constructor(process: Process, user: User, user_perm?: DatabaseUserPerm) {
		this.process = process;
		this.user = user;
		this.view = user_perm?.View === 1 || user.admin;
		this.env = user_perm?.Env === 1 || user.admin;
		this.file = {
			read: user_perm?.FileRead === 1 || user.admin,
			write: user_perm?.FileWrite === 1 || user.admin
		};
		this.manage = user_perm?.Manage === 1 || user.admin;
		this.log = user_perm?.Log === 1 || user.admin;
		this.deploy = user_perm?.Deploy === 1 || user.admin;

		this.opened_files = new Map();
		this.next_open_id = 0;
	}

	destroy(): void {
		const promises: Promise<boolean>[] = [];
		for (const file of this.opened_files.values()) {
			promises.push(this.process.close_file(file));
		}

		Promise.all(promises).catch(console.error);
	}

	static async load(connection: DatabaseConnection, process: Process, user: User): Promise<UserProcess | null> {
		const data = await connection.user_perm_load_specific(user.id, process.id);
		if (data == null)
			return null;

		return new UserProcess(process, user, data);
	}

	static async load_all_for_user(connection: DatabaseConnection, process_map: Map<number, Process>, user: User): Promise<Map<number, UserProcess>> {
		const map = new Map();

		if (user.admin) {
			process_map.forEach(process => {
				map.set(process.id, new UserProcess(process, user));
			});
		} else {
			(await connection.user_perm_load_by_user(user.id)).forEach(row => {
				const process = process_map.get(row.ProcessID);
				if (process == null)
					return;

				map.set(process.id, new UserProcess(process, user, row));
			});
		}

		return map;
	}

	get_app_info(): schema.ProcInfo {
		if (!this.view)
			throw new ApiError(403, "User don't have permission to get info about process");
		return this.process.get_app_info();
	}

	async start(): Promise<void> {
		if (!this.manage)
			throw new ApiError(403, "User don't have permission to start process");
		await this.process.start();
	}

	async stop(): Promise<void> {
		if (!this.manage)
			throw new ApiError(403, "User don't have permission to stop process");
		await this.process.stop();
	}

	async get_monit_info(connection: DatabaseConnection, start_time: number, end_time: number): Promise<{ data: schema.MonitData[]; event: schema.MonitEvent[]; }> {
		if (!this.view)
			throw new ApiError(403, "User don't have permission to get monit info");

		const promises: [Promise<MonitData[]>, Promise<MonitEvent[]>] = [
			MonitData.load_time_range(connection, this.process.id, start_time, end_time),
			MonitEvent.load_time_range(connection, this.process.id, start_time, end_time)
		];

		const promise_data = await Promise.all(promises);

		const data: schema.MonitData[] = promise_data[0].map((item) => {
			return {
				time: item.time,
				cpu: item.cpu,
				memory: item.memory,
				custom: item.custom
			};
		});

		const event: schema.MonitEvent[] = promise_data[1].map((item) => {
			return {
				time: item.time,
				event_type: item.event_type,
				description: item.description
			};
		});

		return {
			data: data,
			event: event
		};
	}

	async open_log(type: schema.LogType): Promise<ProcessOpenFile & { path: string }> {
		if (!this.log)
			throw new ApiError(403, "User don't have permission to open process' log");

		const log_file = this.process.info.logs[type];

		if (log_file == null)
			throw new ApiError(404, "Requested log type hasn't registered file location");

		const open_data = this.process.open_file_unsafe(log_file, false, undefined, undefined);
		return Object.assign({ path: log_file }, await this.register_open_file(open_data));
	}

	get_env(): { [key: string]: string } {
		if (!this.env)
			throw new ApiError(403, "User don't have permission to get env");

		return this.process.config.env ?? {};
	}

	set_env(connection: DatabaseConnection, env: { [key: string]: string }): Promise<void> {
		if (!this.env)
			throw new ApiError(403, "User don't have permission to set env");

		this.process.config.env = env;
		return this.process.save(connection);
	}

	get_deploy_info(): schema.ResponseDeployData {
		if (!this.deploy)
			throw new ApiError(403, "User don't have permission to get deploy info");

		return this.process.deploy.get_deploy_data();
	}

	async generate_deploy_secret(connection: DatabaseConnection): Promise<string> {
		if (!this.deploy)
			throw new ApiError(403, "User don't have permission to generate new deploy secret");

		const new_secret = this.process.deploy.generate_secret();
		await this.process.save(connection);
		return new_secret;
	}

	async generate_deploy_key(connection: DatabaseConnection): Promise<string> {
		if (!this.deploy)
			throw new ApiError(403, "User don't have permission to generate deploy key");

		this.process.deploy.generate_private_key();
		await this.process.save(connection);
		return this.process.deploy.get_public_key() as string;
	}

	set_deploy_info(connection: DatabaseConnection, data: schema.RequestSetDeployData): Promise<void> {
		if (!this.deploy)
			throw new ApiError(403, "User don't have permission to set deploy info");

		this.process.deploy.set_deploy_data(data);
		return this.process.save(connection);
	}

	build(): Promise<void> {
		if (!this.deploy)
			throw new ApiError(403, "User don't have permission to build project");

		return this.process.build();
	}

	list_directory(relpath: string): schema.PathInfo[] {
		if (!this.file.read)
			throw new ApiError(403, "User don't have permission to list directory");

		return this.process.list_directory(relpath);
	}

	create_directory(relpath: string, recursive?: boolean): void {
		if (!this.file.write)
			throw new ApiError(403, "User don't have permission to create directory");

		this.process.create_directory(relpath, recursive);
	}

	move_path(source: string, target: string): void {
		if (!this.file.write)
			throw new ApiError(403, "User don't have permission to move path");

		this.process.move_path(source, target);
	}

	remove_path(relpath: string, recursive?: boolean): void {
		if (!this.file.write)
			throw new ApiError(403, "User don't have permission to remove path");

		this.process.remove_path(relpath, recursive);
	}

	async open_file(relpath: string, write: boolean): Promise<ProcessOpenFile> {
		if (!this.file.read || (write && !this.file.write))
			throw new ApiError(403, "User don't have permission to open file");

		const open_data = this.process.open_file(relpath, write);
		return await this.register_open_file(open_data);
	}

	private async register_open_file(open_data: OpenFileData): Promise<ProcessOpenFile> {
		const index = this.next_open_id++;
		this.opened_files.set(index, open_data);

		const stat = await fstat_promise(open_data.fd);

		return {
			index: index,
			size: stat.size
		};
	}

	async close_file(index: number): Promise<boolean> {
		const open_data = this.opened_files.get(index);
		if (open_data == null)
			throw new ApiError(404, "Requested file is not opened!");

		const retval = await this.process.close_file(open_data);
		this.opened_files.delete(index);
		return retval;
	}

	async read_file_chunk(index: number, start: number, length: number, callback: (data: ReadFileChunk) => void): Promise<void> {
		if (!this.file.read)
			throw new ApiError(403, "User don't have permission to read file");

		const open_data = this.opened_files.get(index);
		if (open_data == null)
			throw new ApiError(404, "Requested file is not opened!");

		let offset = 0;
		const buffer = Buffer.alloc(ConfigLoader.config.api.read_chunk_size);

		let eof = false;
		if (length < 0) // negative length means read to the end of file
			length = Number.MAX_SAFE_INTEGER;

		while (offset < length && !eof) {
			const read_length = Math.min(length - offset, buffer.byteLength);
			const read_data = await read_promise(open_data.fd, buffer, 0, read_length, start + offset);

			eof = read_data.bytesRead != read_length || offset + read_data.bytesRead >= length;

			const data = {
				start: start + offset,
				length: read_data.bytesRead,
				buffer: read_data.bytesRead > 0 ? read_data.buffer.slice(0, read_data.bytesRead) : null,
				eof: eof
			};
			console.log(data);

			callback(data);

			offset += read_data.bytesRead;
		}
	}

	async write_file_chunk(index: number, buffer: Buffer, start: number, length: number): Promise<number> {
		if (!this.file.write)
			throw new ApiError(403, "User don't have permission to write file");

		const open_data = this.opened_files.get(index);
		if (open_data == null)
			throw new ApiError(404, "Requested file is not opened!");

		let written = 0;
		let remaining = length;
		while (remaining > 0) {
			const status = await write_promise(open_data.fd, buffer, written, remaining, start + written);
			written += status.bytesWritten;
			remaining -= status.bytesWritten;
		}

		return written;
	}

	async file_truncate(index: number, length: number): Promise<void> {
		if (!this.file.write)
			throw new ApiError(403, "User don't have permission to write file");

		const open_data = this.opened_files.get(index);
		if (open_data == null)
			throw new ApiError(404, "Requested file is not opened!");

		await ftruncate_promise(open_data.fd, length);
	}

	async watch_file(index: number, change: (event_type: schema.WatchFileEventType, size: number | null) => void, end: () => void): Promise<number> {
		if (!this.file.read)
			throw new ApiError(403, "User don't have permission to watch file");

		const open_data = this.opened_files.get(index);
		if (open_data == null)
			throw new ApiError(404, "Requested file is not opened");

		if (open_data.watcher != null)
			throw new ApiError(400, "Requested file is already watched");

		let filename = open_data.path;
		let switch_user = true;

		if (path.isAbsolute(filename))
			switch_user = false;
		else
			filename = path.join(this.process.dir, filename);

		open_data.watcher = this.process.watch_file_unsafe(open_data.path, switch_user, (event_type) => {
			fstat_promise(open_data.fd).then((stat) => {
				change(event_type, stat.size);
			}).catch((reason) => {
				new ApiError(500, "Error with fstat in watched file", reason).log_error();
				change(event_type, null);
			});
		});

		open_data.watcher_close_callback = end;

		const stat = await fstat_promise(open_data.fd);
		return stat.size;
	}

	unwatch_file(index: number): void {
		if (!this.file.read)
			throw new ApiError(403, "User don't have permission to unwatch file");

		const open_data = this.opened_files.get(index);
		if (open_data == null)
			throw new ApiError(404, "Requested file is not opened");

		if (open_data.watcher == null)
			throw new ApiError(400, "Requested file is not watched");

		this.process.unwatch_file(open_data);
	}

	//TODO: deploy push
}
