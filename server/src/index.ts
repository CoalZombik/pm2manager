import { Main } from './main';

let main: Main | undefined;

function stop(): void {
	if (main == null)
		process.exit(0);
	main.stop();
}

Main.init().then(value => {
	main = value;
	if (typeof process.send === 'function')
		process.send('ready');
}).catch(reason => {
	console.error(reason);
	process.exit(1);
});

process.on('SIGINT', stop);
