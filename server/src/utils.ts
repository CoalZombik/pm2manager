export class UnixUser {
	static privileged_uid: number = process.geteuid();
	static privileged_gid: number = process.getegid();

	/**
	 * Run code as specific Unix user.
	 * 
	 * call must be fully synchonized (no promises/async callbacks)
	 * calling this function recursive leads to unexpected behaviour
	 */
	static run_as_specific<T>(call: () => T, uid: number | undefined, gid: number | undefined): T {
		let output;
		try {
			if (gid != undefined)
				process.setegid(gid);
			if (uid != undefined)
				process.seteuid(uid);

			output = call();
		} finally {
			if (gid != undefined)
				process.setegid(UnixUser.privileged_gid);
			if (uid != undefined)
				process.seteuid(UnixUser.privileged_uid);
		}

		return output;
	}
}
