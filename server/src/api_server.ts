import WebSocket from 'ws';
import ip from 'ip';
import { Api } from "./api.js";
import { Database } from './database.js';
import { Process } from './process.js';

interface ExtendedWS extends WebSocket {
	isAlive: boolean;
	ip?: string;
	api_connect_id: number;
}

export class ApiServer {
	wss: WebSocket.Server;
	api: Api;

	ping_interval: NodeJS.Timeout;

	constructor(port: number, database: Database, process_map: Map<number, Process>, ping_time: number) {
		this.wss = new WebSocket.Server({
			port: port,
			clientTracking: true
		});

		this.api = new Api(database, process_map);
		this.ping_interval = setInterval(() => { this.ping(); }, ping_time);

		this.wss.on('connection', (ws: ExtendedWS, req) => {
			ws.binaryType = 'nodebuffer';

			ws.isAlive = true;
			if (req.socket.remoteAddress != null)
				ws.ip = req.socket.remoteAddress;

			if ((ws.ip == null || ip.isPrivate(ws.ip)) && req.headers['x-forwarded-for'] != null) {
				let forwarded_ip = req.headers['x-forwarded-for'];
				if (Array.isArray(forwarded_ip))
					forwarded_ip = forwarded_ip[0];
				ws.ip = forwarded_ip.split(/\s*,\s*/)[0];
			}

			if (ws.ip == null) {
				ws.terminate();
				throw new Error("Cannot get connected client IP");
			}

			ws.api_connect_id = this.api.connect_user(ws.ip, (message) => { ws.send(JSON.stringify(message)); });
			ws.on('message', (message: string | Buffer) => {
				if (message instanceof Buffer)
					message = message.toString('utf8');

				this.api.process_command(ws.api_connect_id, message);
			});

			ws.on('pong', () => {
				ws.isAlive = true;
			});

			ws.on('close', () => {
				this.api.disconnect_user(ws.api_connect_id);
			});
		});

		this.wss.on('close', () => { clearInterval(this.ping_interval); });
	}

	ping(): void {
		this.wss.clients.forEach((ws) => {
			const extended_ws = ws as ExtendedWS;
			if (!extended_ws.isAlive) {
				console.log("no pong from " + extended_ws.ip);
				extended_ws.terminate();
			} else {
				extended_ws.isAlive = false;
				extended_ws.ping();
			}
		});
	}

	stop(): Promise<void> {
		this.wss.clients.forEach((ws) => {
			ws.close();
		});

		return new Promise((resolve, reject) => {
			this.wss.close((err) => {
				if (err)
					reject(err);
				else
					resolve();
			});
		});
	}
}
