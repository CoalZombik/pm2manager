import ip from 'ip';
import cryptoRandomString from 'crypto-random-string';
import bcrypt from 'bcrypt';
import { ApiError } from './api_error.js';
import { DatabaseConnection, DatabaseToken, DatabaseUser } from './database.js';
import { ConfigLoader } from './config.js';

export class User {
	id: number;
	username: string;
	password: string;
	admin: boolean;
	last_login: number;

	used_token?: string;
	token_valid_time?: number;

	constructor(db_data: DatabaseUser & Partial<DatabaseToken>) {
		this.id = db_data.UserID;
		this.username = db_data.Username;
		this.password = db_data.Password;
		this.admin = db_data.Admin == 1;
		this.last_login = db_data.LastLogin;
		this.used_token = db_data.Token;
		this.token_valid_time = db_data.ValidUntil;
	}

	static async load_by_token(connection: DatabaseConnection, token: string, ip_addr: string): Promise<User> {
		const data = await connection.user_load_by_token(token);

		if (data == null)
			throw new ApiError(401, "Token is not exists");

		if (!ip.isEqual(ip_addr, data.IP))
			throw new ApiError(401, "Invalid IP address");

		const current_time = Date.now() / 1000;

		if (data.ValidUntil < current_time)
			throw new ApiError(401, "Token expired");

		const user = new User(data);
		user.update_last_login(connection, current_time);
		return user;
	}

	static async load_by_login(connection: DatabaseConnection, username: string, password: string, ip_addr: string): Promise<User> {
		const data = await connection.user_load_by_name(username);

		if (data == null)
			throw new ApiError(401, "Invalid username");

		if (!await bcrypt.compare(password, data.Password))
			throw new ApiError(401, "Invalid password");

		const user = new User(data);
		await user.generate_token(connection, Date.now() / 1000 + ConfigLoader.config.api.token_expiration, ip_addr);
		return user;
	}

	async generate_token(connection: DatabaseConnection, valid_until: number, ip_addr: string, token_length = 16): Promise<void> {
		this.used_token = cryptoRandomString({ length: token_length, type: "alphanumeric" });
		this.token_valid_time = valid_until;

		connection.token_insert({
			UserID: this.id,
			ValidUntil: this.token_valid_time,
			Token: this.used_token,
			IP: ip_addr
		});
	}

	async update_last_login(connection: DatabaseConnection, time: number): Promise<void> {
		this.last_login = time;
		connection.user_update_last_login(this.id, time);
	}

	//TODO: Change username/password
}
