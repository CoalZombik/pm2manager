import path from 'path';
import http from 'http';
import express from 'express';
import { json as body_parser_json } from 'body-parser';
import { Process } from './process';
import * as schema from 'pm2manager_schema';

/**
 * This class create http server and manage deploy requests
 */
export class DeployServer {
	process_map: Map<number, Process>;
	server: http.Server;

	constructor(http_port: number, deploy_root_dir: string, process_map: Map<number, Process>) {
		this.process_map = process_map;

		const express_instance = express();

		//Json body parser with raw buffer wrapping for signature validation
		express_instance.use(body_parser_json({
			verify: (req: http.IncomingMessage & { raw_body: Buffer }, res, buf) => { req.raw_body = buf; }
		}));

		express_instance.post(path.join(deploy_root_dir, "github/:id"), (req, res) => { this.parse_request_github(req, res); }); //FIXME: check if deploy method is checked
		express_instance.post(path.join(deploy_root_dir, "api/:id/:event"), (req, res) => { this.parse_request_api(req, res); });
		this.server = express_instance.listen(http_port);
	}

	parse_request_github(req: express.Request & { raw_body?: Buffer }, res: express.Response): void {
		const process = this.process_map.get(parseInt(req.params.id));
		if (!process || process.deploy.method != schema.DeployMethod.github) {
			res.sendStatus(404);
			return;
		}

		if (!req.raw_body || !process.deploy.validate_sign(req.raw_body, req.get('X-Hub-Signature'))) {
			res.sendStatus(403);
			return;
		}

		const event_type = req.get('X-GitHub-Event');
		if (event_type == null) {
			res.sendStatus(400);
			return;
		}

		const deploy_data = process.deploy.parse_github(event_type, req.body);
		if (!deploy_data) {
			res.sendStatus(400);
			return;
		}

		res.sendStatus(200);

		// run deploy and ignore error (because is already logged)
		process.deploy.run(deploy_data).catch(() => { return; });
	}

	/**
	 * Parse api request from http server (called from inside)
	 * 
	 * @param {express.Request} req 
	 * @param {express.Response} res 
	 */
	parse_request_api(req: express.Request & { raw_body?: Buffer }, res: express.Response): void {
		const process = this.process_map.get(parseInt(req.params.id));
		const event = req.params.event;
		if (!process) {
			res.sendStatus(404);
			return;
		}

		if (!req.raw_body || !process.deploy.validate_sign(req.raw_body, req.get('X-Signature'))) {
			res.sendStatus(403);
			return;
		}

		const deploy_data = process.deploy.parse_api(event, req.body);
		if (!deploy_data) {
			res.sendStatus(400);
			return;
		}

		// run deploy and ignore error (because is already logged)
		process.deploy.run(deploy_data).catch(() => { return; });
	}

	/**
	 * Stop a server
	 * 
	 * @returns {Promise<void>}
	 */
	stop(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.server.close((err) => {
				if (err)
					reject(err);
				else
					resolve();
			});
		});
	}
}