import fs from 'fs';
import path from "path";
import mysql from "promise-mysql";
import prompt from "prompt";
import fetch from 'node-fetch';
import chalk from 'chalk';
import Ajv, { JSONSchemaType, DefinedError } from "ajv";
import { Pm2ManagerConfig as Config } from '../../tmp/config';
import { ConfigLoader } from "../config";
import { DatabaseInfo, DatabaseVersion } from '../database';
import { Process } from "../process";

class SetupWizard {
	readonly schema: JSONSchemaType<Config>;
	readonly config_path: string;

	constructor(config_path: string) {
		prompt.start();
		prompt.message = "";
		prompt.delimiter = "";

		this.schema = JSON.parse(fs.readFileSync("config.schema.json", { encoding: 'utf8' }));

		this.config_path = config_path;
	}

	async run(): Promise<void> {
		try {
			await this.validate_config();
			await this.update_db();
			await this.check_yarn();
		} catch (error) {
			this.log_error_header("Setup failed");
			console.error("Reason: %s", error instanceof Error ? error.message : error);
		}
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	log_header(message: any, ...params: any[]): void {
		console.log(chalk.bold(chalk.green("> ") + message), ...params);
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	log_error_header(message: any, ...params: any[]): void {
		console.error(chalk.bold(chalk.red("> ") + message), ...params);
	}

	async prompt(property: prompt.RevalidatorSchema): Promise<string | prompt.RevalidatorSchema> {
		const prompt_schema: prompt.Schema = {
			properties: { value: property }
		};

		try {
			const value = await prompt.get(prompt_schema);
			return value.value;
		} catch (_e) {
			console.log(""); // prompt doesn't write new line
			throw _e;
		}
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	private object_walk(obj: any, path: string, last_skip?: number): any {
		const parts = path.split('/');

		try {
			let output = obj;
			for (let i = 1; i < parts.length - (last_skip ?? 0); i++) {
				output = output[parts[i]];
			}
			return output;
		} catch (_e) {
			return undefined;
		}
	}

	/**
	 * Check validity of configuration and prompt user for requiered changes
	 */
	async validate_config(): Promise<void> {
		this.log_header("Checking server configuration");

		const ajv = new Ajv();
		const validate = ajv.compile<Config>(this.schema);

		let config = {};
		try {
			config = JSON.parse(fs.readFileSync(this.config_path, { encoding: 'utf8' }));
		} catch (_e) {
			// config file can be non-existing
		}

		while (!validate(config)) {
			if (validate.errors == null)
				throw new Error("Validate fails but error array is empty");

			const error = validate.errors[0] as DefinedError;

			let full_path = error.instancePath;
			let key_name = error.instancePath.split('/').at(-1) as string;

			const config_part = this.object_walk(config, error.instancePath, error.keyword != 'required' && error.keyword != 'additionalProperties' ? 1 : 0);
			let schema_part = this.object_walk(this.schema, error.schemaPath, 1);

			if (config_part == null || schema_part == null)
				throw new Error("Unexpected error response from JSON validation");

			if (error.keyword == 'additionalProperties') {
				delete config_part[error.params.additionalProperty];
				continue;
			}

			if (error.keyword == 'required') {
				key_name = error.params.missingProperty;
				full_path += "/" + key_name;
				schema_part = schema_part.properties[key_name];

				if (schema_part.type == 'object') {
					this.object_walk(config, error.instancePath)[error.params.missingProperty] = {};
					continue;
				}
			}

			const prompt_property: prompt.RevalidatorSchema = {
				type: schema_part.type,
				pattern: schema_part.pattern,
				description: "Enter value:",
				message: "Invalid value",
				minimum: schema_part.minimum,
				maximum: schema_part.maximum,
				exclusiveMinimum: schema_part.exclusiveMinimum,
				exclusiveMaximum: schema_part.exclusiveMaximum,
				divisibleBy: schema_part.multipleOf,
				minItems: schema_part.minItems,
				maxItems: schema_part.maxItems,
				uniqueItems: schema_part.uniqueItems,
				enum: schema_part.enum,
				default: schema_part.default,
				format: schema_part.format
			};

			console.log(chalk.bold.cyan(full_path));
			if (schema_part.description != null)
				console.log(schema_part.description);
			config_part[key_name] = await this.prompt(prompt_property);
			console.log(chalk.dim("----------"));
		}

		this.log_header("Configuration is valid");

		fs.writeFileSync(this.config_path, JSON.stringify(config, undefined, '\t'));
		fs.chmodSync(this.config_path, "600");
		ConfigLoader.config = config;
	}

	async connect_db(): Promise<mysql.Connection> {
		const mysql_config = ConfigLoader.config.mysql;

		const connection = await mysql.createConnection({
			host: mysql_config.host,
			port: mysql_config.port,
			user: mysql_config.user,
			password: mysql_config.password,
			multipleStatements: true
		});

		return connection;
	}

	async load_db_version(connection: mysql.Connection): Promise<number | undefined> {
		try {
			await connection.query("USE `" + ConfigLoader.config.mysql.database + "`");
			const info = await connection.query("SELECT * FROM `info` LIMIT 1") as DatabaseInfo[];

			if (info.length == 0)
				return undefined;

			return Number(info[0].DbVersion);
		} catch (_e) {
			return undefined;
		}
	}

	async init_db(connection: mysql.Connection, force?: boolean): Promise<void> {
		this.log_header("Initialising database");
		const database_name = ConfigLoader.config.mysql.database;

		const databases = await connection.query("SHOW DATABASES") as { Database: string }[];
		const already_exists = databases.filter((row) => row.Database == database_name).length > 0;

		if (already_exists) {
			// show confirmation about dropping database
			if (!(force ?? false)) {
				const value = await this.prompt({
					type: "boolean",
					description: "Database already exists and must be dropped. Do you want to continue?",
					message: "Invalid value",
					default: false
				});

				if (!(value as boolean))
					throw new Error("Database initialization cancelled");
			}

			await connection.query("DROP DATABASE `" + database_name + "`");
		}

		await connection.query("CREATE DATABASE `" + database_name + "`");
		await connection.query("USE `" + database_name + "`");

		const db_schema = fs.readFileSync("db/current.sql", { encoding: 'utf8' });

		await connection.query(db_schema);

		this.log_header("Database initialized");
	}

	async update_db(): Promise<void> {
		this.log_header("Checking database");
		const connection = await this.connect_db();

		let current_version = await this.load_db_version(connection);

		while (current_version != undefined && current_version < DatabaseVersion) {
			this.log_header(`Updating database from version ${current_version}`);

			const update_sql = fs.readFileSync(`db/update.${current_version}.sql`, { encoding: 'utf8' });
			await connection.query(update_sql);
			current_version = await this.load_db_version(connection);
		}

		if (current_version == undefined)
			await this.init_db(connection);
		else if (current_version > DatabaseVersion)
			throw new Error(`Unexpected version of database: ${current_version} (expected ${DatabaseVersion})`);

		this.log_header("Database is up-to-date");

		await connection.end();
	}

	async check_yarn(): Promise<void> {
		this.log_header("Checking yarn executable");
		try {
			Process.init_yarn();
		} catch (_e) {
			console.warn("Invalid yarn executable");
			console.log("Downloading https://github.com/yarnpkg/berry/raw/master/packages/berry-cli/bin/berry.js");
			const file = await fetch("https://github.com/yarnpkg/berry/raw/master/packages/berry-cli/bin/berry.js");

			if (!file.ok)
				throw new Error("Downloading yarn failed");

			const target_dir = path.dirname(Process.yarn_exec);
			fs.mkdirSync(target_dir, { recursive: true });
			fs.writeFileSync(Process.yarn_exec, Buffer.from(await file.arrayBuffer()));

			fs.chmodSync(Process.yarn_exec, '755');
		}

		this.log_header("Yarn is valid");
	}
}

const wizard = new SetupWizard('config.json');
wizard.run();
